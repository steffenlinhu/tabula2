// Dieser Code sorgt fuer ein periodisches Scrollen einer HTML-Datei
// Kontext: s_html.py in tabula.info, aber auch für andere Systeme geeignet
// Autor: C. Bienmueller
// Rechte: public domain (da trivial)
// Funktion: Wartet 3s, scrollt dann alle 23ms um 1 PixelZeile bis ...
//           Seite am Ende, dann nach 3s Rückfahrt zum Seitenanfang in 2 s
//           Nach weiteren 3 Sekunden erneutes Scrollen
// Eine HTML-Seite muss dieses Script im head einbinden mit :
//          <script src="scroll.js" type="text/javascript"></script>
// und aufrufen ueber:
//          <body onload="scrollstart()" >

var linetime=31;            //ms für eine Pixelzeile - die Scrollgeschwindigkeit!

function scrollstart() {
    // Startet nach 3 Sekunden das Scrolling
    document.body.style.position="absolute";
    document.body.style.top="-8px";
    window.setTimeout(function(){ scrollinit(); },3000);
}

function scrollinit() {
    // Berechnet zu scrollende Höhe als Differenz aus Fenster- und Dokumentenhöhe
    // Wegen evtl. Fenstergrößenänderung wird dies jedesmal neu berechnet...
    var scrollhoehe=getDocHeight()-getViewportHeigth();
    if (scrollhoehe>0) {
        // Nur wenn gescrollt werden muss, so wird eine entsprechnd lange CSS-Transition und dann die neue Höhe festgelegt
        var scrolltime_ms =linetime*scrollhoehe;
        document.body.style.transition=             "top " + Math.round(scrolltime_ms/1000).toString() + "s linear 0s"; 
        document.body.style["-webkit-transition"]=  "top " + Math.round(scrolltime_ms/1000).toString() + "s linear 0s"; 
        document.body.style.top="-"+scrollhoehe.toString()+"px";
    }
    // Nach der vorberechneten Zeit soll dann zum Anfang zurück gescrollt werden
    window.setTimeout(function(){ scrollreset(); },scrolltime_ms+3000);
}
function scrollreset() {
    // Scrollt wieder an den Anfang der Seite (in 2s)
    document.body.style.transition=             "top 2s ease 0s"; 
    document.body.style["-webkit-transition"]=  "top 2s ease 0s"; 
    document.body.style.top="-8px";
    // ruft 5s später erneutes Scrollen auf (2s Scrolling, 3s Warten)
    window.setTimeout(function(){ scrollinit(); },5000);
}

function getViewportHeigth() {
/* from http://stackoverflow.com/questions/3437786 */
var w = window,
    d = document,
    e = d.documentElement,
    g = d.getElementsByTagName('body')[0],
    x = w.innerWidth || e.clientWidth || g.clientWidth,
    y = w.innerHeight|| e.clientHeight|| g.clientHeight;
return y;
}
function getDocHeight() {
/* from http://james.padolsey.com/javascript/get-document-height-cross-browser/ */
    var D = document;
    return Math.max(
        Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
        Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
        Math.max(D.body.clientHeight, D.documentElement.clientHeight)
    );
}

