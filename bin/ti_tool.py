#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Batteries (included)
import sys, traceback
import ast, codecs
import os.path, pwd
import sqlite3, time
import socket

import gettext

#TI-Import 
import ti_lib
# minimale(!!) tabula.info-Module
try:
    import config_db
except Exception as e:
    panic("ti_tool failed on imports",e)

'''
________________________________________________________________________
 ti_tool.py                                                            
 EN: This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß          
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 ti_tool.py kann von der Kommandozeile/ Shellskripten und per CGI
    aufgerufen werden.

________________________________________________________________________
'''

try:
    if __name__ == '__main__':
        exitcode=0
        ti_cnf=config_db.TI_Config()
        if len(sys.argv)<2 or len(sys.argv)>3: # also kein legitimer cli-Aufruf, gehe von CGI aus und zeige Status der Anzeigerechner an
            import ti
            ti_ctx=ti.master_init()
            ti_cnf.client_show_status(ti_ctx) 

        elif sys.argv[1]=="--set_semaphore" and len(sys.argv)>2:
            exitcode=ti_cnf.check_n_set_semaphore(sys.argv[2]) 

        elif sys.argv[1]=="--clear_semaphore" and len(sys.argv)>2:
            ti_cnf.clear_semaphore(sys.argv[2]) # endet mit exit(code)

        elif sys.argv[1]=="--s_html_convert":
            import s_html_upload
            exitcode=s_html_upload.convert_files(ti_lib.get_ti_basepath())

        elif sys.argv[1]=="--set_ti_lastupload":
            ti_cnf.set_ti_lastupload()

        elif len(sys.argv)==2:
            if sys.argv[1]=="basepath":
                print(ti_lib.get_ti_basepath())
            else:
                print(ti_cnf.get_db_config(sys.argv[1],default=''))         

        elif len(sys.argv)==3:
            ti_cnf.set_db_config(sys.argv[1],sys.argv[2])
        exit(exitcode)
except Exception as e:
    ti_lib.panic("ti_tool failed!",e)

