#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
________________________________________________________________________
 manage_persons.py                                                            
 This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 Diese Datei ist Teil von tabula.info, das Freie Software gemäß
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 manage_persons.py ermöglicht den Personenruf per Browser zu verwalten
 und nutzt dabei u.a. die Datenstruktur P_Call aus persons.py
________________________________________________________________________
'''
# Batteries
import string
import time
import cgitb
cgitb.enable()
# TI
import ti
import konst
import persons # wg. Objektdefinition P_Call
import login    

def print_goto_messages(resultatmeldung=""):
    forwarder='''<head>
    <meta http-equiv="refresh" content="{0}; URL=messages.py?manage_mode=1">
    <title>tabula.info - forwarding...</title>
    </head>'''
    if len(resultatmeldung)!=0:
        delay=10
    else:
        delay=0
    ti.prt(forwarder.format(str(delay)))
    if delay>0:
        ti.prt("<h2>Fehler: "+resultatmeldung+"</h2>")

def print_edit_persons_form(ti_ctx,pnr):
    targets=[]
    for t in ti_ctx.cnf.get_db_config("Ziele").split(";"):
        t=t.strip()
        if t:
            targets.append(t)
    targets.sort()
    targetselect=""
    for t in targets:
        if t[0]=="-":
            targetselect+="<option selected>"+t[1:]+"</option>"
        else:
            targetselect+="<option>"+t+"</option>"
    skeleton='''
      <div class="firstcontent">
        <h1>Bearbeiten: Personenruf</h1>
      </div>
      <div style="float:left;width:45em;margin:1em;">
        <div class="persons"  style="border:solid 1px blue;"><br>
        <h3 class="mint">Neuer Eintrag:</h3>
            <form action="/cgi-bin/manage_persons.py" method="post" style="text-align: center; margin-top: 10px;">
                <input name="todo" value="add" type="hidden">
                <input name="pnr" value="{pnr}" type="hidden">
                <input name="number_2_delete" value="" type="hidden">
                <table>
                    <tr>
                        <td>Klasse</td>
                        <td>Name,&nbsp;Vorname</td>
                        
                    </tr>
                    <tr>
                        <td><input name="klasse"  id="klasse" size="5" maxlength="5" type="text"></td>
                        <td><input name="name"  id="name" size="30" maxlength="200" type="text"></td>
                        
                    </tr>
                    <tr>
                        <td>Ziel</td>
                        <td>(evtl.&nbsp;Grund)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        
                        
                    </tr>
                    <tr>
                        <td><select name="target" size="10">
                              {targetselect}
                            </select></td>
                        <td>(<input name="reason"  id="reason" size="10" maxlength="20" type="text">)
                        <br><br><br><br><br>
                            <input value="Absenden" type="submit" style="color:white;background-color:blue;">                        
                        </td></tr>
                </table>
            </form>
            <p style="text-align:right">
            <a href="#" class="tooltip" data-tooltip="
            Unbedingt die ersten beiden Felder ausfüllen.
            Die Einträge werden später erst nach Klasse, dann alphabetisch sortiert.
            Man kann mehrere Schüler der gleichen Klasse gleichzeitig eingeben, wenn man sie mit Doppelpunkten oder Strichpunkten trennt!
            Die Angabe der Klasse hilft, damit auch Lehrer die Schüler erinnern können.
            Eine Eingabe, bei der Klasse oder Name leer bleibt, wird kommentarlos ignoriert!
            Tricks: Zwischen den Eingabefeldern mit Tabulatortaste wechseln, in der Auswahlliste mit den Pfeiltasten wählen,
            mit der Eingabetaste absenden....">Anleitung</a>
            </p>
        </div>
      </div>
      <div style="float:left;width:45em;margin:1em;">
        <div class="persons" style="border:solid 1px blue;"><br>
        <h3 class="mint">Aktuelle Einträge:</h3>
            {ps}
        <br>
        </div>
      </div>
      
    '''

    persons_string=""
    i=0
    timestamp_now=ti_ctx.sys.get_epoche()
    po_array=persons.pdb_get_persons(ti_ctx)
    for p in po_array:
        persons_string+='\t<p class="mint"><a href="manage_persons.py?number_2_delete='+ \
                str(p.get_rowid())+ \
                '&todo=delete"><img src="/static/erase.png" width="150" height="19" border="0" alt="loeschen"> </a>&nbsp;' + \
                p.get_visible_longname()+' <big>&rArr;</big> '+ \
                p.get_target()+' seit '+ \
                str(p.get_age_hours(timestamp_now))+' Stunden</p>\n'
        i+=1
    extrastyle=''
    '''                 <style type="text/css">
                        table       { width:0 ; align:left; background:#BBF;}
                        td          { border:0px solid #000; vertical-align:top; overflow:hidden;  background:#BBF;}
                        body, div   {font-size:16px;}   
                        h1 {padding:0.5em;}
                        body        { padding:0.5em;background-color:white; overflow:auto;}     </style>'''
    #ti_ctx.print_html_head( extrastyle=extrastyle, \
    #           title="tabula.info - Personen verwalten")
    ti_ctx.print_html_head(ismanagement=True)

    ti.prt(skeleton.format(ps=persons_string,pnr=str(pnr),targetselect=targetselect))
    

def stripbadletters(eingabe):
    goodletters=' 0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!()*+,-.:?@_äöüÄÖÜß€àáâèéêïìíîòóôùúû'
    return''.join([letter for letter in eingabe if letter in goodletters]).strip()

def do_manage(ti_ctx):
    ti_ctx.check_management_rights(konst.RECHTEPersonenruf)
    pnr=ti_ctx.req.get_cgi_int("pnr",1)


    n2d=ti_ctx.req.get_cgi_int("number_2_delete",-1)
    resultatmeldung=""
    anzahl=0
    try:
        
        new_name=ti_ctx.req.get_cgi("name")
        new_klasse=ti_ctx.req.get_cgi("klasse")
        new_target=ti_ctx.req.get_cgi("target")
        new_reason=ti_ctx.req.get_cgi("reason")

        if len(new_name)>3 or len(new_klasse)>1 or len(new_target)>3:
        
            new_name=":".join(new_name.split(";"))
            new_name=stripbadletters(new_name)

            new_klasse=stripbadletters(new_klasse)
            if len(new_klasse)>1 and new_klasse[0].isdigit():
                new_klasse=new_klasse[0].upper()+new_klasse[1:].lower()

            new_target=stripbadletters(new_target)

            new_reason=stripbadletters(new_reason)
            
            if len(new_name)>3 and len(new_target)>3:
                c=(new_klasse+' ')[0]
                if c.isdigit() and c!="0" and not ( len(new_klasse)>1 and new_klasse[:2].isdigit() ):
                    new_klasse="0"+new_klasse
                if len(new_reason)>2:
                    new_reason=" ("+new_reason+")"
                else:
                    new_reason=""
                names=new_name.split(":")
                delta=0
                while len(names)>0:
                    nextname=names.pop()
                    if len(nextname)>2:
                        persons.pdb_write_person(ti_ctx,new_klasse+": "+nextname,new_target+new_reason,int(ti_ctx.sys.get_epoche()))
                        anzahl+=1
            else:
                resultatmeldung="-Nicht alle notwendigen Felder (Name und Ziel) ausgefüllt! "
        elif n2d>=0:
            resultatmeldung=persons.pdb_delete_person(ti_ctx,n2d)
        if anzahl==1:
            resultatmeldung='+Es wurde ein Personenruf eingetragen'
        elif anzahl>1:
            resultatmeldung='+Es wurden '+str(anzahl)+' Personenrufe eingetragen'
            
        
    except():
        resultatmeldung="-I/O-Fehler beim Namen einlesen! "

    #Nun die eigentlichen HTML-Ausgaben
        
    if len(resultatmeldung)==0:
        print_edit_persons_form(ti_ctx,pnr)

    else:
        ti_ctx.set_erfolgsmeldung(resultatmeldung)
        ti_ctx.print_html_head(title="tabula.info - Forwarding...",url="manage_persons.py",timeout=0)

    ti_ctx.print_html_tail()

if __name__ == '__main__':
    ti_ctx=ti.master_init()
    try:
        do_manage(ti_ctx)
    except Exception as e:
        ti_lib.panic('manage_persons abgestuerzt:',ti_lib.beschreibe_exception(e))
