#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
________________________________________________________________________
 ti_init.py                                                            
 EN: This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß          
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 ti_init.py führt zwei Arten von Initialisierung durch, 
            die erst via WSGI einen Unterschied machen:
    - für das System, einmalig bis zum Neustart des Skripts
    - für den Clientrequest
________________________________________________________________________
'''
# Batteries
import cgitb
cgitb.enable()

import ast
import codecs
import cgi
import datetime, time
import locale
import os
import os.path
import sqlite3
import string
import sys
from abc import ABCMeta, abstractmethod
import inspect # für die debugging-Funktion
# TI
import ti_lib
 
try:
    import konst
    import client
except Exception as e:
    ti_lib.panic('ti_init.py: IMPORT fehlgeschlagen',e,'')

 
#################################################    
### hier beginnt der Code zur Initialisierung ###
#################################################
class TI_System():
    """Enthält alle Informationen für das "hochgefahrene" TI-System
       Leider müssen Datenbank-initialisierungen threadweise getrennt vorgenommen werden.
    """
    def __init__(self):
        try:
            locale.setlocale(locale.LC_ALL,('de_DE', 'UTF8'))   
        except:
            ti_lib.panic('Kann nicht auf deutsche Einstellungen schalten',BaseException("Es muss Linux mit deutschem locale installiert sein!") )
        self.start_epoche=ti_lib.get_now()

        
        try:
            self.cdb_conn=sqlite3.connect(ti_lib.get_ti_datapath('content.sq3'))
            cursor=self.cdb_conn.cursor()
            cursor.execute("""CREATE TABLE IF NOT EXISTS messages (
                                flag INTEGER,
                                targetday INTEGER,
                                created INTEGER,
                                deleted INTEGER,
                                lasthour INTEGER,
                                target INTEGER,
                                headline TEXT,
                                details TEXT,
                                color TEXT,
                                author TEXT ) """)
            cursor.execute("""CREATE TABLE IF NOT EXISTS persons (
                                longname TEXT,
                                target TEXT,
                                timestamp INTEGER) """)
            cursor.close()
            self.cdb_conn.commit()
        except Exception as e:
            ti_lib.panic('Content-DB konnte nicht initialisiert werden',e)
    

    def get_epoche(self):
        return self.start_epoche

       
class TI_Response():
    """sammelt Informationen für die Response
       inkl. Fehlermeldungen"""
    def __init__(self,epoche,debug2stderr=False): 
        self.debug_messages=''
        self.start_epoche=epoche
        self.debug2stderr=debug2stderr
    #######################################
    # einfache Debuggingmöglichkeit
    def debug(self,*args,use_caller_caller=False):
        values=[ str(t) for t in args ]
        caller=inspect.currentframe().f_back.f_code if not use_caller_caller else inspect.currentframe().f_back.f_back.f_code 
        file_name=caller.co_filename.split('/')[-1]
        func_name='.'+caller.co_name
        caller_name=' '*(14-len(file_name))+file_name+func_name+' '*(18-len(func_name))
        if len(values)>1    and (values[1] and values[1][ 0] not in ':=') \
                            and (values[0] and values[0][-1] not in ':='):
            values[0]+=" ="
        self.debug_messages+= '<!-- ti-Debug von {} meldet: {} -->\n'.format( \
            caller_name, ' '.join(values))
        if self.debug2stderr:
            sys.stderr.write(' '.join(values)+'\n')
            sys.stderr.flush()

    # muss am Ende händisch aufgerufen werden, kann aber auch mehrfach geschehen
    def debug_flush(self):
        ti_lib.prt('''<div style="visibility:hidden">\n''') # nur falls die ausgegebenen Strings die Kommentare aufbrechen
        ti_lib.prt(self.debug_messages)
        ti_lib.prt('''</div>''')
        self.debug_messages=''
        
    def debug_timestamp(self,eventname,timestamp=0):
        
        if timestamp:
            self.debug(eventname,str(timestamp-self.start_epoche),True)
        else:
            self.debug(eventname,str(time.time()-self.start_epoche),True)
        
# danke Dan Bader
# Abstract Base Class stellt sicher, dass beide Klassen dasselbe (Grund)-Interface bereitstellen
class TI_Request_ABC(metaclass=ABCMeta):
    @abstractmethod
    def get_cgi(self,parameter,default=""):
        pass
        
    @abstractmethod
    def get_cgi_bool(self,parameter,default=False):
        pass
        
    @abstractmethod
    def get_cgi_int(self,parameter,default=0):
        pass
        
    @abstractmethod
    def get_cgi_object(self,parameter):
        pass
        
class TI_Request_CGI(TI_Request_ABC):
    """enthält alle Informationen für den aktuellen Aufruf per CGI"""
    def __init__(self,ti_cnf,ti_res): 
        self.client_name=""
        self.cgiFS=cgi.FieldStorage()
        try:
            ip_address=cgi.escape(os.environ["REMOTE_ADDR"])
        except:
            ip_address='127.0.0.1'
        self.client=client.TI_Client(ip_address,ti_cnf,ti_res)
        # für messages.py
        self.mess_manage_mode=False
        self.mess_protokoll_mode=False
        
    ################################
    # CGI-Werte handhaben
    def get_cgi(self,parameter,default=""):
        return self.cgiFS.getfirst(parameter,default)

    def get_cgi_bool(self,parameter,default=False):
        return ti_lib.strg2bool(self.get_cgi(parameter),default)
        
    def get_cgi_int(self,parameter,default=0):
        return ti_lib.strg2int(self.get_cgi(parameter),default)
        
    # für Uploads
    def get_cgi_object(self,parameter):
        if parameter in self.cgiFS:
            return self.cgiFS[parameter]
        else:
            return None

class TI_Request_WSGI(TI_Request_ABC):
    """enthält alle Informationen für den aktuellen Aufruf per WSGI via werkzeug"""
    def __init__(self,request,ti_cnf,ti_res): # das request-Objekt kommt von "werkzeug", das ti_res...ponseobjekt muss vorher initialisiert worden sein
        self.client_name=""
        # hier nicht: self.cgiFS=request.FieldStorage()
        self.request=request
        # Zukunft?: self.s_interface=s_any.S_Interface()
        try:
            ip_address=request.remote_addr
        except:
            ip_address='127.0.0.1'
        self.client=client.TI_Client(ip_address,ti_cnf,ti_res)
        # quasiglobale Variablen für messages.py
        self.mess_manage_mode=False
        self.mess_protokoll_mode=False
        
    ################################
    # CGI-Werte handhaben
    def get_cgi(self,parameter,default=""):
        return self.request.args[parameter]

    def get_cgi_bool(self,parameter,default=False):
        return ti_lib.strg2bool(self.get_cgi(parameter),default)
        
    def get_cgi_int(self,parameter,default=0):
        return ti_lib.strg2int(self.get_cgi(parameter),default)
        
    # für Uploads (ungetestet 2020-01-12)
    def get_cgi_object(self,parameter):
        if parameter in self.request:
            return self.request[parameter]
        else:
            return None

