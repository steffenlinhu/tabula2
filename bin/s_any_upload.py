#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
________________________________________________________________________
 s_any_upload.py                                                            
 This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 Diese Datei ist Teil von tabula.info, das Freie Software gemäß
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 s_any_upload.py ermöglicht den Vertretungsplan und/oder andere Dateien 
                      in unterschiedlicher Form per Browser hochzuladen
________________________________________________________________________

 
'''
# Batteries
import cgi
import cgitb; cgitb.enable()
import os, sys, glob
import subprocess, time 
import shutil
import codecs
# TI
import ti
import konst



HTML_TEMPLATE = '''
<div class="firstcontent" style="margin-bottom:0.5em">
    <h1>{}</h1>
</div>
<div class="upload">
    {{erfolgsmeldung}}
    <form action="s_any_upload.py" method="POST" enctype="multipart/form-data">
    <div ><br>
    <table style="width:auto;">
    <tr><td colspan=2 style="padding-right:3em;">
            <H3>{}:</H3>
            {{filelist}}
        </td><td colspan=2 style="padding-right:3em;">
            <H3>{}:</H3>
            {{fixlist}}
        </td><td  colspan=2>
            <H3>{}:</H3>
            {{toollist}}
        </td></tr>
    <tr><td>
            <br><br></td>
            <td style="padding-right:3em;">
                <input  style="color:red" type="submit" name="delete" value=" {} " /><br><br></td><td></td>
            <td style="padding-right:3em;">
                <input  style="color:red" type="submit" name="delete_fix" value=" {} " /><br><br></td><td></td>
            <td>
                <input  style="color:red" type="submit" name="delete_tool" value=" {} " /><br><br>
            </td>
        </tr>
    
    <tr><td>
            &nbsp;1.) {}:<br>
            </td><td><input name="upload_files" type="file" multiple="multiple" /><br>&nbsp;</td><td></td><td></td>
        </tr>
    <tr><td>
            2.a) {}<br></td><td style="padding-right:3em;">
            {{upload_buttons}}
            </td>
            <td>
            2.b) {}<br><br></td><td style="padding-right:3em;">
            <input type="submit" name="festlegen" value=" {} " /><br>
            <small>({}) </small><br><br></td>

            <td style="overflow:visible;"> <a href="#" class="tooltip" data-tooltip="
            {}">{}</a></td><td></td>
        </tr>
    
    </table>
    </div>
    </form>
    
    <H3>Info:</H3>
    {{info}}
    
</div>
'''.format(_('Pläne aktualisieren'),_('Hier hochgeladene Pläne'),_('Hier hochgeladene Fixpläne'),_('Per Tool hochgeladene Pläne'),
    _('Lösche hochgeladene Pläne'),_('Lösche hochgeladene Fixpläne'),_('Lösche Toolpläne'),_('Dateien zum "hier Hochladen" wählen'),
    _('Damit bestehende Pläne...'),_('oder als Fixpläne...'),_('...festlegen'),_('ein Pool fester Pläne wird hiermit ersetzt'),_('''Sie können z.B. mit einem FTP-Programm oder per ssh(fs) 
            ebenfalls Dateien hochladen. 
            Ziel muss /home/tabula/upload sein.
            Dort vorhandene Dateien werden hier aufgelistet und mitverwendet.
            Sie können hier nur gelöscht werden!'''),
    _('Erklärung für den Upload'))

HTML_UPLOAD_BUTTONS='''
            <input type="submit" name="hochladen" value=" {} " /> <small>{}</small><br>
            <input type="submit" name="ergaenzen" value=" {} " /> <small>{}</small><br><br>
            '''.format(_('...alle ersetzen'),_('(alle Pläne werden vor dem Upload gelöscht)'),_('...nur ergänzen'),_('(nur gleichnamige Pläne werden ersetzt)'))

H_T_INFO=_('''<br><br><p><small>Bitte beachten:</p>
    <ul><li>Die Reihenfolge der Dateien ist beliebig</li>
        <li>Die Namen müssen keinen Regeln entsprechen (können aber), die Erweiterung (.pdf etc.) ist jedoch wichtig</li>
        <li>Werden Namen mit Datum, "heute" oder "morgen" verwendet, so haben diese Priorität, ansonsten gilt die lexikalische Reihenfolge</li>
        <li><b>Fixpläne</b> werden wie Pläne ausgewertet und dienen als statischere Ergänzung bei dynamisch wechselnden Plänen</li>
    </ul>
    <ul>Dateinamenslogik:<li><b>2013-01-13</b>.pdf wird bis zu diesem Tag angezeigt.
    <li>plan_<b>heute</b>_mensa_M.pdf wird nur am Upload-Tag angezeigt (in der M-Anzeigegruppe)</li>
    <li>Endungen: Bspw. subst_001<b>_L</b>.htm wird nur bei Lehrern angezeigt. Erkannte Endungen sind _Q, _A, _B, _M und _L</li>
    <li>Gibt es mindestens eine Datei für z.B. Lehrer, so wird/werden nur dies/e dort angezeigt.<br>
     Gibt es keine Datei für eine solche Anzeigegruppe, so werden an den entsprechenden Clients die allgemeinen Dateien (ohne Endung) angezeigt.</li>
    </ul>
    <ul>Dateitypen:
    <li>HTML: wird ggf. ergänzt (für scrolling), refresh entfernt und direkt angezeigt. Siehe auch CSS!</li>
    <li>PDF: wird seitenweise in ein in PNG, ggf. mit horizontalem Scrolling, konvertiert.</li>
    <li>PNG, GIF, JPG werden 1:1 übernommen.</li>
    <li>CSV: werden als TurboPlaner (Haneke)-Export interpretiert und als HTML dargestellt.</li>
    <li>CSS: wird 1:1 kopiert unter Beibehaltung des Dateinamens. Damit können HTML-Dateien problemlos eine Formatierung erhalten.</li>
    <li>URL: Textdatei, welche eine Zeile mit einer URL enthält. Diese wird angezeigt - der Client muss sie im Netzwerk direkt erreichen können!</li>
    
    </ul>''')
def get_filelist(path):
    result=[]
    line='<td>🗋{}</td><td> <small>{}</small></td>'
    try:
        for filename in os.listdir(path):
            result.append(line.format(filename,ti.get_datestring_from_epoche(os.path.getmtime(path+filename))))
    except:
        result=['<td>'+_('Fehler bei der Dateiauflistung, existiert das Verzeichnis?')+'</td>']
    return '<table><tr>'+'</tr><tr>'.join(result)+'</tr></table>'

def get_weblist():
    return get_filelist(ti.get_ti_datapath("upload/"))

def get_fixlist():
    return get_filelist(ti.get_ti_datapath("fix/"))
    
def get_toollist():
    return get_filelist(TOOLPATH)
    
def print_html_form(ti_ctx,erfolg,fnamen):
    """This prints out the html form. Note that the form is a self-posting form.
    In other words, this cgi both displays a form and processes it.
    """

    if erfolg:
        if fnamen!="keine":
            erfolgsmeldung='''
                <div class="erfolg">
                    {}:{anzahl}
                    <ul><li>
                    {namen}
                    </li></ul>
                </div><br>'''.format(_('Anzahl hochgeladener Dateien'),anzahl=len(fnamen),namen='</li><li>'.join(fnamen))
        else:
            erfolgsmeldung='''
                <div class="erfolg">
                    {}
                </div><br>'''.format(_('Es wurden keine Dateien hochgeladen!'))
    else:
        erfolgsmeldung=''
    ti_ctx.print_html_head(ismanagement=True)
    quelle=ti_ctx.cnf.get_db_config('s_any_quelle')
    upload_buttons='<b>'+_("Pläne werden automatisch<br>heruntergeladen von<br>der Quelle: ")+quelle+'</b>' if quelle else HTML_UPLOAD_BUTTONS
    ti.prt(HTML_TEMPLATE.format(erfolgsmeldung=erfolgsmeldung,filelist=get_weblist(),fixlist=get_fixlist(),toollist=get_toollist(),info=H_T_INFO,upload_buttons=upload_buttons))
    ti_ctx.print_html_tail()    
    
def save_uploaded_files(ti_ctx):
    """This saves the files uploaded by an HTML form.
    The form_field is the name of the file input field from the form.
    If no file was uploaded or if the field does not exist then
    this does nothing.
    """
    success=False
    filenamelist=[]
    anzahl=0
    dodelete=True
    usepath=ti.get_ti_datapath("upload/")
    
    if ti_ctx.req.get_cgi("delete_tool",""):
        usepath=TOOLPATH
        ti_ctx.res.debug("ToolDateien löschen")
        
    if ti_ctx.req.get_cgi("festlegen","") or ti_ctx.req.get_cgi("delete_fix",""):
        usepath=ti.get_ti_datapath("fix/")
        ti_ctx.res.debug("FixDateien löschen")
        
    if ti_ctx.req.get_cgi("ergaenzen",""):
        dodelete=False
        ti_ctx.res.debug("Dateien nicht löschen, sondern ergänzen")
    else:
        ti_ctx.res.debug("Dateien löschen")
    
    if ti_ctx.req.get_cgi("delete","") or ti_ctx.req.get_cgi("delete_fix","") or ti_ctx.req.get_cgi("delete_tool",""):
        try:
            for filename in os.listdir(usepath):
                os.remove(usepath+filename)
                ti_ctx.res.debug('Datei loeschen',usepath+filename)
            success=True
        except:
            ti_ctx.res.debug("Dateien löschen fehlgeschlagen!!!")
    else:        
        filefield=ti_ctx.req.get_cgi_object('upload_files')
        try:
            if not isinstance(filefield, list): # nach http://stackoverflow.com/questions/12240267/uploading-multiple-files-via-single-form-field-via-python-cgi
                filefield = [filefield]
            first=True
            for fileitem in filefield:
                if fileitem.filename:
                    if first: # nun kommen wirklich Dateien rein - VOR der ersten wird das Verzeichnis geleert - inkl vplansemaphore.txt
                        for filename in os.listdir(usepath):
                            if dodelete or filename=='vplansemaphore.txt':
                                os.remove(usepath+filename)
                                ti_ctx.res.debug('Datei loeschen',usepath+filename)
                        first=False
                    fn = ti.secure_filename(fileitem.filename)
                    ti_ctx.res.debug("hochgeladen wird",fn)
                    filenamelist.append(fn)
                    # save file
                    with open(usepath + fn, 'wb') as f:
                        shutil.copyfileobj(fileitem.file, f)
                        success=True
        except:
            ti_ctx.res.debug("Any-Upload"," keine Datei übergeben") 
            filenamelist='keine'

    fout= open(usepath+"vplansemaphore.txt",'wb')
    fout.write(b'semaphore um valide Dateien anzuzeigen')
    fout.close()
    if success:
        ti_ctx.cnf.set_db_config('s_any_force_convert',1)
        ti_ctx.cnf.set_db_config('background_timeout',0)
    return success,filenamelist

def do_upload(ti_ctx):
    ti_ctx.check_management_rights(konst.RECHTEVertretungsplan)
    ti_ctx.res.debug("Any-Upload","gestartet")  
    erfolg,fnamen=save_uploaded_files(ti_ctx)
    print_html_form(ti_ctx,erfolg,fnamen) 


if __name__ == '__main__':
    ti_ctx=ti.master_init()
    TOOLPATH="/home/tabula/upload/"

    do_upload(ti_ctx)
