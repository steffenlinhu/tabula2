#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
________________________________________________________________________
 pages.py                                                            
 EN: This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 pages.py legt Seiten an, die aus je 1-2 Plänen bestehen. 
 Es handelt sich um eine Zusammenfassung der 
    analyse-schedule und compose-schedule - Funktionen für alle
    darzustellenden Seiten auf einmal.
________________________________________________________________________
'''
# Batteries
import os
import cgitb; cgitb.enable()
# TI
import ti
import ti_lib

class Page():
    def __init__(self,content,cm):
        """erfasst den oder die Inhalte, die eine Plan-Seite anzeigen soll.
            Inhalt ist hier der korrekte HTML-Schnipsel, der ein div definiert, das im Seitenaufbau eingebunden wird.
        """
        self.content=content # Inhalt der Seite 
        self.cm=cm          # Spaltenmodus: 1= nur links, 2=volle Breite
        
    def get_dict(self):
        """liefert den anzuzeigenden Inhalt als HTML-Schnippsel zurück, 
            der in die div-struktur eingebunden werden kann."""
            # liefere Tupelersatz dict 
        return ({"content":self.content,"timeout":20})

    def get_cm(self):
        return self.cm

schnippsel_full='''            
            <iframe class="plan_iframe" src="{url}" id="s_html" frameborder="0">Browser veraltet?</iframe>\n'''
schnippsel_double='''
            <div id="contentpl" class="tiP_Row">
                <iframe class="plan_iframe" src="{url}" id="s_html" frameborder="0">Browser veraltet?</iframe>
            </div>
            <div id="contentpr" class="tiP_Row">
                <iframe class="plan_iframe" src="{url2}" id="s_html2" frameborder="0">Browser veraltet?</iframe>
            </div>\n'''
schnippsel_left='''
            <div id="contentpl" class="tiP_Row">
                <iframe class="plan_iframe" src="{url}" id="s_html" frameborder="0">Browser veraltet?</iframe>
            </div>\n'''
            
class PageAssembly():
    
    def __init__(self,ti_ctx):
        self.ti_ctx=ti_ctx
        self.left=-1     # -1: nicht gesetzt, 0...9 vplannummer
        self.right=-1    # -1: nicht gesetzt, 0...9 vplannummer
        self.zweitage=False
        self.cm=0
        
        self.schedule_c3_body=''
        self.clientgruppennummer=0
        self.vplanliste=[]
        self.pages=[]    #  nimmt Objekte der Klasse page auf
        
    def analyse_whole_schedule(self,flag_phase0_fullpage=True):
        vplanliste,debuginfo=self.ti_ctx.cnf.get_s_any_vplan_liste(self.clientgruppennummer)
        vplanlen=len(vplanliste)
        self.debug(f'get_s_any_vplan_liste liefert: {debuginfo} Laenge: {vplanlen}')
        clientgruppennummer=self.ti_ctx.get_client_max_groups_number()
        self.debug(f'pages_aws: clientgruppennummer{str(clientgruppennummer)}')
        query=''
        skip_plan=False
        for plan in range(0,min(10,vplanlen)): 
            kein_zweitplan= self.ti_ctx.cnf.get_db_config_bool("flag_s_any_kein_zweitplan",False) and \
                            self.ti_ctx.cnf.get_db_config_bool("Flag_ShowInfos",False)
            self.debug(f"pages_aws: Analyse von Plan {plan}")
            if skip_plan:
                self.debug(f"pages_aws: skipping")
                skip_plan=False

                
                        
            elif vplanliste[plan].spalten==2:  # [  - p - ]  passt nur alleine drauf
                schnipps=schnippsel_full.format(url=vplanliste[plan].url+query)
                self.pages.append(Page(schnipps,2))
                self.debug(f"pages_aws: {plan} alleine und breit")
              
            elif plan==vplanlen-1 or  vplanliste[plan+1].spalten==2:
                                                # [  p  ?  ]  kein weiterer Plan (passt drauf)
                schnipps=schnippsel_left.format(url=vplanliste[plan].url+query)
                self.pages.append(Page(schnipps,1))
                self.debug(f"pages_aws: {plan} alleine nur links")
              
            elif vplanliste[plan].spalten==1 and vplanliste[plan+1].spalten==1:
                                                # [  p p+1 ]  zwei Pläne
                schnipps=schnippsel_double.format(url=vplanliste[plan].url+query,url2=vplanliste[plan+1].url+query)
                self.pages.append(Page(schnipps,2))
                skip_plan=True
                self.debug(f"pages_aws: {plan} mit {plan+1}  nebeneinander")
              

 
    def debug(self,*args):
        self.ti_ctx.res.debug(*args,use_caller_caller=True)
 

   
