#!/usr/bin/python3
'''
________________________________________________________________________
 s_turbo_import.py                                                            
 This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 Diese Datei ist Teil von tabula.info, das Freie Software gemäß          
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 s_turbo_import.py liest aus der Datei vplan.csv die Informationen um 
 die Datenstrukturen gemäß def_schedule.py aufzubauen. schedule.py kann
 die Daten dann in HTML anzeigen lassen.
 * Diese Datei muss für jedes Programm zu Vertretungsplanung *
 *  bzw. dessen Exportdatei angepasst werden.                *
________________________________________________________________________
'''
# Batteries
import string
import codecs
import cgitb
import sys, csv
cgitb.enable()
# TI
import ti, ti_lib
from s_turbo_def import *

# Keiner hat behauptet, dass die folgende Datei leicht zu lesen ist
# Sie ist speziell auf das KGA abgestimmt
# !! diese Datei wird nicht internationalisiert, das kga-spezifisch !!
        
KOMMENTARERSETZUNG= [   ("Zusammenlegung","Zusammen­legung"),
                        ("geänderter Raum","Raum­änderung"),
                        ("!","! "),
                        ("! !","!!")]
KLASSENERSETZUNG=[ ("senz",".") ]

#Die folgenden Strings sind Schlüsselwörter, damit die sie enthaltenden Zeilen im Internet nicht angezeigt werden!                              
AUSSCHLUESSE_BEI_REDUZIERT=["krank:","dbf.:","fortbildung:","intern:","dbf:","(1)","(2)","(3)","(4)","(5)","(6)","(7)","(8)","(9)","mutterschutz"]


ERSETZEN_BEI_REDUZIERT={ } # Bsp:{ '11_d_nnnv' : '1d_1' }
 
LEHRERERSETZUNG={"Debes Carla":"Debes Carla","Debes Carolin":"Debes Carol."}
    
def cleanteacher(lehrer):
    lehrer=lehrer.replace(',',' ').strip()
    if not lehrer: return ''
    while '  ' in lehrer:
        lehrer=lehrer.replace('  ',' ')
    if lehrer in LEHRERERSETZUNG:
        return LEHRERERSETZUNG[lehrer]
    lelist=lehrer.split(' ')
    if len(lelist)>1:
        if lelist[1]=='Dr.' and len(lelist)>2:
            return lelist[0]+' Dr. '+lelist[2][0]+'.'
        return lelist[0]+' '+lelist[1][0]+'.'
    if lehrer.startswith('---'):
        return '-->'
    return lehrer

def customize_table(import_table):
    '''Hier findet die Anpassung der csv-Datei des Stundenplan-Programms zur Darstellung in tabula.info statt'''
    global schedule_obj
    global debugging
    lastday=0
    
    # alle zeilen bearbeiten
    for row in import_table:
        #Vermeidung von Laufzeitfehlern und dauernden Abfragen
        #Spezifisch für die Eingangsdaten
        while len(row)<=9:
            row.append("0")
        newrow=[""]*10
        #jetzt die Fallunterscheidungen für die verschiedenen Spalten
        if row[0][0].isdigit():
            #Spalte 0 enthält das Datum, was hier in das tabula.info-eigene Format konvertiert wird:
            day=ti.get_dayofepoche_from_string(row[0])
            if debugging: 
                debug("Zeile für Tag ",ti.get_string_from_dayofepoche(day)+" aus "+row[0])
            if lastday!=day:
                if lastday!=0:
                    schedule_obj.add_day(day_obj)
                day_obj=S_Day(day)
                lastday=day
            #Spalte 2 enthält die betroffene Unterrichtsstunde
            newrow[0]= row[2]
            if newrow[0].isdigit(): expire=int(newrow[0])
            else:                   expire=0
            #Spalte 3 enthält die betroffenen Klassen
            # kürze deren Bezeichnungen
            # (dabei ist Klassenliste eine Liste aller betroffenen Klassen von Zeile)
            for ist,soll in KLASSENERSETZUNG:
                row[3]=row[3].replace(ist,soll)

            klassenliste=row[3].split()
            klassennr=0
            for i in range(len(klassenliste)):
                # Bezeichnungen kürzen
                klassenliste[i]=klassenliste[i].split('-')[0].lower()
                # aus "8a 8b" die 8 extrahieren 
                if  klassenliste[i][0].isdigit():
                    if len(klassenliste[i])>1:
                        if klassenliste[i][1].isdigit():
                            if klassennr==0:
                                klassennr=int(klassenliste[i][:2])  # zweistellige Klassennummer erfassen
                            if int(klassenliste[i][0])==0:
                                klassenliste[i]=''+klassenliste[i][1:] #bei führender Null diese entfernen
                    if klassennr==0:
                        klassennr=int(klassenliste[i][0])   # einstellige Klassennummer erfassen
                else:
                    pass # wg. Klammern - Test 2018-09-14
                    #klassenliste[i]=''
                
            newrow[1]=klassennr
            newrow[2]=' '.join(klassenliste)
            
            #Spalte 4 enthält das betroffene Fach und wird gekürzt
            #if '11' in row[4]:
            fach=row[4].split()
            for i in range(len(fach)):
                if len(fach[i])>5:
                    fach[i]=fach[i][:5]
                    if fach[i]=='NATTE':
                        fach[i]='NuT'
            newrow[3]=' '.join(fach)
            
            #Spalte 5 enthält den zu vertretenden Lehrer als Kürzel
            newrow[4]=row[5]
            keyfach=row[4][:5]
            if keyfach=="SPK": keyfach="spo"
            key=row[3]+"_"+keyfach
            key=key.lower()
            if debugging: debug("substitute-key:",key)
            if key in ERSETZEN_BEI_REDUZIERT:
                newrow[5]="Q"+row[3]+": "+ERSETZEN_BEI_REDUZIERT[key]
                newrow[3]=ERSETZEN_BEI_REDUZIERT[key]
            elif row[3] in ['11','12']:
                newrow[3]=key
                newrow[5]=key
            else:
                newrow[5]=newrow[2]+": "+newrow[3]
                        
            #Spalte 6 enthält den vertretenden Lehrer im Vollnamen.
            # Der Vorname wird abgekürzt, wobei Dr. erkannt wird
            newrow[6]=cleanteacher(row[6])    
            if not newrow[6] and key in ERSETZEN_BEI_REDUZIERT:
                newrow[6]='<i><small>KursNr. beachten</small></i>'

            #Spalte 7 enthält die Raumbezeichnung
            newrow[7]=row[7]
                        
            #Spalte 8 enthält einen Kommentar (ggf. zum ganzen Tag) und wird zu einem HTML-String mit Zeilenvorschub zusammengefügt
            commentline=row[8]
            #ti_debug("commentline",commentline)
            #ti_debug("commentline0-22",commentline[:22])
            highlight=0 if row[9].startswith("0") else 1 # Haneke-Markierung für neue Einträge
            if expire<100:
                for ist,soll in KOMMENTARERSETZUNG:
                    commentline=commentline.replace(ist,soll)
                commentline=commentline.replace('\\n','')
                if commentline.find('statt')>0:
                    teile=commentline.split("statt",1)
                    commentline='{}<small>statt{}</small>'.format(teile[0],teile[1])
                if commentline.find('**')>=0:   # Flag für neue Einträge
                    if commentline.find('**gr')>=0: #grün
                        highlight=4
                    elif commentline.find('**ge')>=0: #gelb
                        highlight=3
                    elif commentline.find('**ro')>=0: #rot
                        highlight=1
                    else:
                        highlight=1
                    commentline=commentline.split("**")[0]

                if commentline.find("fällt aus, vorgezogen")>=0 and row[6]=="":
                    newrow[6]="(vorgez. "+commentline[22:]+")"
                    commentline='fällt aus'
                    if not highlight: highlight=2 # grüne Schrift
                elif commentline[:11]=="fällt aus," and row[6]=="":
                    newrow[6]="("+commentline[12:]+")"
                    commentline='fällt aus'
                    if not highlight: highlight=2 # grüne Schrift
                elif commentline.find("fällt aus")>=0:
                    if not highlight: highlight=2 # grüne Schrift

            elif expire in (100,101,102): # ab hier für einen Tageskommentar (100) vertretende Lehrer (101) oder fehlende Lehrer (102)
                comment=commentline.split('\\n')
                for i in range(len(comment)):
                    if len(comment[i].strip())==0:
                        comment[i]='<!--feed-->'
                    else:
                        if comment[i][0]==' ':
                            comment[i]='      '+comment[i].lstrip()
                        elif comment[i][:3]=='---':
                            comment[i]=' --> '
                        comment[i]="&nbsp;&nbsp;&nbsp;".join(comment[i].rstrip().split("   "))
                if len(comment)>1 and expire==100:
                    commentline=""
                    firstbold=True
                    dekofettunterstrichen=' style="text-decoration:underline; font-weight:bold;"'
                    dekofett=' style="font-weight:bold;"'
                    dekorot='class="s_t_fresh"'
                    dekogelb='class="s_t_gelb"'
                    dekogruen='class="s_t_gruen"'
                    for i in range(0,len(comment)):
                        deko=''
                        if comment[i].find("**")>=0:
                            if comment[i].find("**gr")>=0:
                                deko=dekogruen
                                comment[i]=comment[i].replace("**gr","")
                            elif comment[i].find("**ge")>=0:
                                deko=dekogelb
                                comment[i]=comment[i].replace("**ge","")
                            else:
                                deko=dekorot
                                comment[i]=comment[i].replace("**","")
                        
                        doppel=comment[i].split(":",1)
                        if len(doppel)==2:
                            if firstbold:
                                comment[i]='<span '+deko+dekofettunterstrichen+'>'+doppel[0]+':</span>'
                                if deko:
                                    comment[i]+='<span '+deko+'>'+doppel[1]+'</span>'
                                else:
                                    comment[i]+=doppel[1]
                                firstbold=False
                            else:
                                comment[i]='<span '+deko+dekofett+'>'+doppel[0]+':</span>'
                                if deko:
                                    comment[i]+='<span '+deko+'>'+doppel[1]+'</span>'
                                else:
                                    comment[i]+=doppel[1]
                        elif deko:
                            comment[i]='<span '+deko+'>'+comment[i]+"</span>"
                        if comment[i]=='<!--feed-->':
                                firstbold=True
                        if i>0:
                            if comment[i]!='<!--feed-->' or comment[i-1]!='<!--feed-->':
                                commentline+='<BR>'+comment[i]
                        else:
                            commentline=comment[0]
                elif len(comment) and expire==101:
                    comment=commentline.split(' ')
                    commentstunde=""
                    if comment[-1].strip().startswith('('):
                        commentstunde=comment[-1]
                        comment.pop(-1)
                    commentline=cleanteacher(" ".join(comment)).replace(" ","&nbsp;")+'<small>&nbsp;'+commentstunde+'</small>'
                    debug("101-Kommentar",commentline)
                else:
                    commentline=comment[0]
            newrow[8]=commentline
            
            
            
            if debugging: debug("s_turbo_import",newrow)
            if int(newrow[0])<100:
                day_obj.add_row(S_Row(newrow,expire,highlight=highlight,sortvalue=int(newrow[0])*100+int(newrow[1])),deduplicatecell=0)
            else:
                day_obj.add_comment(commentline,int(expire-100))
                
        #except:    # passiert, wenn gar keine indizierbare Tabelle vorhanden ist, z.B. Leerzeilen in der csv-Datei
        #   debug("Importfehler bei ",row)
        #   debug("Exception:",sys.exc_info())
            
    if lastday!=0:
        schedule_obj.add_day(day_obj)
    #krönender Abschluss:
    schedule_obj.analyze_days()
                

def read_table(previewflag=False,filename=''):
    global table_gelesen
    global debugging
    
    if debugging: 
        ti_lib.log("s_t_i.read_table startet")
        debug("read_table mit preview",previewflag)
    if table_gelesen:
        if debugging: debug("read_table für "+filename+" wurde erneut aufgerufen und daher abgebrochen")
        return
    try:
        if previewflag:
            reader=csv.reader(codecs.open(ti.get_ti_datapath("tmp/vplan.tmp"),mode="r",encoding='iso-8859-1'),delimiter="|")
        else:
            pathundname=ti.get_ti_datapath("tmp/vplan.csv") if not filename else filename
            reader=csv.reader(codecs.open(pathundname,mode="r",encoding='iso-8859-1'),delimiter="|")
        import_table=[]
        for row in reader:
            import_table.append(row)
    
    except:
        ti_ctx.print_html_head()
        ti.prt('''<H1>I/O-Fehler - Plan konnte nicht eingelesen werden</H1>
            Evtl. wurde noch kein Vertretungsplan hochgeladen oder<br>
             er hat falsche Leseberechtigungen?<br>
             Evtl. sollte nicht s_turbo für den Vertretungsplan verwendet werden?-> ti.config ändern!''')
        #FIXME?? ti.ti_res.debug_flush()
        exit()
    if debugging: 
        ti_lib.log("s_t_i.read_table hat Zeilen eingelesen:",len(import_table))
    customize_table(import_table)
    table_gelesen=True

        
def get_schedule_obj():
    return schedule_obj
    
schedule_obj=S_Schedule()

schedule_obj.set_column_names(   ["Std","KlNr.","Kl.","Fach","Plan","Kurs","Vertret.","Raum","Bem.","XXX"])
schedule_obj.set_column_display([[ True, False,  True, True,  True,  False, True,      True,  True ,False], \
                                 [ True, False,  False,False, False, True,  True,      True,  True ,False]])
schedule_obj.set_column_bold(   [[ False,False,  True, False, False, False, False,     False, False,False], \
                                 [ False,False,  True, False, False, False, False,     False, False,False]])
schedule_obj.set_column_small(  [[ False,False,  False,False, False, False, False,     False, True ,False], \
                                 [ False,False,  False,False, False, False, False,     False, True ,False]])
        


debugging=True

table_gelesen=False

if __name__ == '__main__':
    ti_ctx=ti.master_init()
    ti_ctx.print_html_head("s_turbo_import.py selftest")
    debugging=False
    read_table()
    ti.prt("<H3>Bitte Quelltext betrachten</H3>")
    ti_ctx.print_html_tail()
