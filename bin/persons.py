#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
________________________________________________________________________
 persons.py                                                            
 This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 Diese Datei ist Teil von tabula.info, das Freie Software gemäß
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 persons.py definiert die Datenstruktur P_Call und gibt ansonsten
 eine Liste der zu rufenden Schüler als HTML-Code zurück
________________________________________________________________________
'''
# Batteries
import string
import cgitb
cgitb.enable()
import sys
import codecs
import sqlite3
# TI
import ti_lib

class P_Call(object):
    def __init__(self, rowid, longname, target, timestamp):
        self.rowid=rowid
        self.longname=longname
        self.target=target
        self.timestamp=int(timestamp)
        
    def get_rowid(self):
        return self.rowid
        
    def get_true_longname(self):
        return self.longname
    
    def get_visible_longname(self):
        return self.longname if not self.longname[0] in ["0"] else self.longname[1:]
    
    def get_target(self):
        return self.target
    
    def get_timestamp(self):
        return self.timestamp
        
    def get_age_hours(self,timestamp):
        return  (timestamp-self.timestamp)//3600


def iterate_persons(ti_ctx,reverse_flag=False):
    po_array=pdb_get_persons(ti_ctx)
    if (len(po_array)>0 ) :
        itmess='<div class="persons"><table cellspacing="0"><tr >\n'
        itmess+='\t<td nobord="true"><h3><u>Personenruf'
        if reverse_flag:
            itmess+=' (höchste Klasse zuerst)'
        itmess+=':</u></h3> \n\t<table cellspacing="4" width="auto">\n'
        
        alternating_colors=0
        row=0
        timestamp_now=ti_ctx.sys.get_epoche() 
        bewegung='<big>&rArr;</big>'
        if reverse_flag:
            po_array.reverse()
        for p in po_array:
            agehours=p.get_age_hours(timestamp_now)
            
            itmess+='\t<tr>\t'                      # Zeilenanfang
            farbe=''
                
            if agehours<2:                          # grün für Frischlinge
                farbe='green'
            elif agehours>24:                       # Rot einfärben nach 24 h
                farbe='red'
            elif agehours>8 and ti_lib.get_akt_hour()>=9:  # Orange einfärben ab 9 Uhr wenn nicht weniger als 8 h alt
                farbe='gelb'
            else:
                farbe='blue'
            pklasse,pnamen=p.get_visible_longname().split(':',1)
            if len(pklasse.strip())>0:
                pklasse+=':'
            pimpedklasse=pklasse if len(pklasse)<4 else '<small>{}</small>'.format(pklasse)
            namensanzeige='<td nobord="true" style="text-align:right;width:2em;vertical-align:top;"><big>&nbsp;</big><span alt_person="{farbe}">{klasse}</span></td><td nobord="true"><span alt_person="{farbe}">{name}</span>'.format(farbe=farbe,klasse=pimpedklasse,name=pnamen)
            pimpedtarget=p.get_target() if len(p.get_target())<11 else '<small>{}</small>'.format(p.get_target())
            itmess+= namensanzeige+'&nbsp;'+bewegung+' '+pimpedtarget
            itmess+= '</td></tr>\n'
                
        itmess+='\t</table>\n\t</td></tr></table>\n'
        itmess+='</div>\n'
        
        # End of iterating persons
        return itmess
    return ''
    
def pdb_get_persons(ti_ctx):
    '''gibt alle zu den Parametern passenden Personen als Objekt-Array zurück'''
    cursor=ti_ctx.sys.cdb_conn.cursor()
    cursor.execute("""
            SELECT ROWID, longname, target, timestamp
            FROM persons
            ORDER BY longname""")
    po_array=[]
    for row in cursor:
        rowid, longname, target, timestamp = row
        pobj=P_Call(rowid, longname, target, timestamp)
        po_array.append(pobj)
    cursor.close()
    return po_array

def pdb_write_person(ti_ctx,longname, target, timestamp):
    cursor=ti_ctx.sys.cdb_conn.cursor()
    cursor.execute("""INSERT INTO persons
                    (longname, target, timestamp) 
                    VALUES (?,?,?)""",
                    (longname, target, timestamp))
    cursor.close()
    ti_ctx.sys.cdb_conn.commit()
    
def pdb_delete_person(ti_ctx,rowid):
    cursor=ti_ctx.sys.cdb_conn.cursor()
    cursor.execute("""DELETE FROM persons WHERE rowid=?""",(rowid,))
    anzahl=cursor.rowcount
    cursor.close()
    ti_ctx.sys.cdb_conn.commit()
    if anzahl==1:
        return '+Es wurde ein Personenruf gelöscht'
    return '+Es wurden '+str(anzahl)+' Personenrufe gelöscht'

if __name__ == '__main__':
    import ti
    ti_ctx=ti.master_init()
    ti_ctx.print_html_head("persons.py SELFTEST")
    ti.prt(iterate_persons(ti_ctx))
    ti_ctx.print_html_tail()
