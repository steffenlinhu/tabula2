#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
________________________________________________________________________
 s_any_download.py                                                            
 EN: This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 s_any_download.py wird vom Hintergrundprozess background.py aufgerufen
________________________________________________________________________

Es wird ein Pfad aus der config.db geholt (s_any_quelle),
die Anwesenheit von vplansemaphore.txt überprüft,
von dort Dateien gemäß s_any_dateinamen heruntergeladen
und im any/upload-Verzeichnis abgelegt
'''

# Batteries
import urllib.request as ur
from urllib.error import URLError
import os,time,codecs,shutil
# TI
import ti_lib


def do_any_download(ti_cnf):
    erfolg=False
    meldung=''
    anyupload_path=ti_lib.get_ti_datapath()+'upload/'
    quelle=ti_cnf.get_db_config('s_any_quelle')
    if not quelle:
        return False,_('Keine Downloadquelle konfiguriert')
    if not (    quelle.lower().startswith('file://')  or \
                quelle.lower().startswith('ftp://')  or \
                quelle.lower().startswith('http://')  or \
                quelle.lower().startswith('https://')        ):
        return False, _('Downloadquelle falsch spezifiziert: Protokoll fehlt oder falsch')
    if not quelle.endswith('/'):
        quelle=quelle+'/'
    dateimuster=ti_cnf.get_db_config('s_any_dateinamen').split(';')
    dateimuster.append('vplansemaphore.txt') # damit es als letztes heruntergeladen wird -
                        # also ggf. auch nicht, wenn die Quelle bis zum Downloadende nicht mehr verfügbar ist
    #print(dateimuster)
    ti_lib.log('DWN: Teste '+quelle+dateimuster[-1])
    if existiert_datei(quelle+dateimuster[-1]): # wenn vplansemaphore.txt existiert
        ti_lib.log('DWN:',quelle+dateimuster[-1],'gefunden')
        loesche_alle_uploads()
        for datei in dateimuster:
            zieliteratoren= [   ['',True], ['_Q',True], ['_A',True], ['_B',True], ['_M',True], ['_L',True]  ]
            if '{}' in datei:
                for i in range(0,10):
                    if download_von(quelle,datei.format(i), zieliteratoren, not i): # bei i=0 hat man ein Freispiel, 1 wird auf jeden Fall auch probiert
                        erfolg=True
            elif '{datum}' in datei:
                for i in range(0,27):
                    idatum=ti_lib.isodatum_heute_plus_delta(i)
                    if download_von(quelle,datei.format(datum=idatum), zieliteratoren, True): # keine Konsequenzen bei Nichtfinden
                        erfolg=True
            else:
                if download_von(quelle,datei,zieliteratoren,True):
                    erfolg=True
        meldung='Download(s) erfolgreich' if erfolg else 'vplansemaphore.txt gefunden, aber keine weiteren Dateien'
        ti_lib.log('DWN:',meldung)
        return erfolg,meldung
    else:
        meldung='vplansemaphore.txt nicht gefunden - Vermutlich keine korrekte Verbindung zur Downloadfreigabe'
        ti_lib.log('DWN:',meldung)
        return False,meldung
    

def download_von(pfad,name,zieliteratoren,freispiel):
    erfolg=False
    anyupload_path=ti_lib.get_ti_datapath()+'upload/'
    root,ext=os.path.splitext(name)
    for d in range(len(zieliteratoren)) if name !='vplansemaphore.txt' else [0]: # speziellen Dateinamen nicht iterieren
        if zieliteratoren[d][1]:        # Flag, dass dieser Iterator noch benutzt werden soll
            i=zieliteratoren[d][0]      # String, der die Kennzeichnug für ein Ziel darstellt
            url1=pfad+root+i+ext        # komplette url mit pfad/dateiname_<Ziel>.extension
            url2=pfad+i[-1]+'/'+root+ext if i else '' # ggf. komplette mit url mit pfad/<Ziel>dateiname.ext
            ziel=anyupload_path+root+i+ext  # global festgelegtes upload-Verzeichnis
            #ti_lib.log(name+' '+i+':\n '+url1+' '+url2)
            try:
                with ur.urlopen(url1) as src:
                    with open(ziel,'wb') as dest:
                        shutil.copyfileobj(src,dest)
                        ti_lib.log('DWN:',url1,' heruntergeladen')
                        erfolg=True
                        
            except URLError:
                if url2:
                    try:
                        with ur.urlopen(url2) as src:
                            with open(ziel,'wb') as dest:
                                shutil.copyfileobj(src,dest)
                                ti_lib.log('DWN:',url2,'heruntergeladen')
                                erfolg=True
                                
                    except URLError:
                        if not freispiel:
                            zieliteratoren[d][1]=False # probier's nicht mehr
                elif not freispiel:
                    zieliteratoren[d][1]=False # probier's nicht mehr
    return erfolg
        
def existiert_datei(url):
    try:
        with ur.urlopen(url) as f:
            r=f.read(1)
    except:
        return False
    return True
    
def loesche_alle_uploads(nurheute=False):
    anyupload_path=ti_lib.get_ti_datapath()+'upload/'
    try:
        os.remove(anyupload_path+'vplansemaphore.txt')
        time.sleep(2)
    except:
        pass
        
    for filename in os.listdir(anyupload_path):
        if not nurheute or 'heute' in filename.lower():
            try:
                os.remove(anyupload_path+filename)
            except:
                pass
            

if __name__ == '__main__':
    ti_ctx=ti.master_init()
    succ,meld=do_any_download()
    if succ:
        ti_ctx.cnf.set_db_config('s_any_force_convert',1)
    print('Erfolg: '+str(succ))
    print('Text  : '+meld)
    
    
