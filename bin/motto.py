#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
________________________________________________________________________
 motto.py                                                            
 This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 Diese Datei ist Teil von tabula.info, das Freie Software gemäß          
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 motto.py liefert ein Wochenmotto
________________________________________________________________________
'''
# Batteries
# (nix)
# TI
import ti
import konst

def get_motto(ti_ctx):
    motto=ti_ctx.cnf.get_db_config('ti_motto')
    if motto!="off" and len(motto)>0:
        if len(motto)<100:
            content='''
            <div  id="wochenmotto" >
                <p><small><small><small>Das Wochenmotto:</small></small></small></p>
                <h1>{m}</h1>
            </div>\n'''.format(m=motto)
        else:
            content= '''
                <div id="wochenmotto">
                    <p><small><small><small>Das Wochenmotto:</small></small></small></p>
                    <h1  style="font-size:97% ">{m}</h1>
                </div>      
                '''.format(m=motto)
        return content
    return ""

def manage_motto(ti_ctx):
    m=ti_ctx.req.get_cgi("motto")
    if len(m)>1:
        ti_ctx.cnf.set_db_config('ti_motto',m[:64])
        ti_ctx.set_erfolgsmeldung('+Motto wurde gesetzt' if m!='off' else '+Motto wurde abgeschaltet')
    skeleton='''
      <div class="firstcontent" style="margin-bottom:0.5em;"><h1>Motto setzen</h1></div>
      <div id="wochenmotto" style="text-align: center;"><br>
        <form action="/cgi-bin/motto.py" method="post" style="text-align: center; margin-top: 10px; font-size=20px;">
            <input name="motto" value="{mottowert}" type="text" size="48" maxlength="64" />(max 64 Zeichen)
            <br>
            <input value="Absenden" type="submit">
        </form><br><br>oder
        <form action="/cgi-bin/motto.py" method="post" style="text-align: center; margin-top: 10px; font-size=20px;">
            <input name="motto" value="off" type="hidden"><br>Das Wochenmotto vorläufig<br>
            <input value="Abschalten" type="submit">
        </form><br>
       </div>
     </body>
    '''
    ti_ctx.print_html_head(ismanagement=True)
    mw=ti_ctx.cnf.get_db_config('ti_motto')
    ti.prt(skeleton.format(mottowert=mw if mw!='off' else ''))
    ti_ctx.print_html_tail()


if __name__ == '__main__':
    ti_ctx=ti.master_init()
    if ti_ctx.check_management_rights(konst.RECHTEMeldungen | konst.RECHTEMeldungenerweitert):
        manage_motto(ti_ctx)  
    else:
        ti_ctx.print_html_head("motto.py SELFTEST")
        ti.prt(get_motto(ti_ctx))
        ti_ctx.print_html_tail()
