#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
________________________________________________________________________
 frames.py                                                            
 EN: This file is part of tabula.info, which is free software under              
      the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß
          der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 frames.py liefert die automatisch generierten und (via HTML-refresh)
    einander folgenden Seiten, die tabula.info ausmachen.
    Nur diese Seite wird von den Browsern zur Darstellung aufgerufen!                                         
________________________________________________________________________

 Der komplette Funktionsumfang von frames.py ist bisher nicht vollständig
    dokumentiert - er ändert sich aber gerade mit jeder Version. In Kürze:
    - Zuerst wird eine Seite mit i.d.R. Meldungen (messages) und
      Personenruf (persons) dargestellt.
    - Nach z.B. 20 Sek. wird die heutige, (bzw. früheste Vertretungsplanseite
      ab heute gerechnet) dargestellt.
    - gibt es weitere Vertretungspläne, so folgen diese, bevor wieder die Startseite erscheint.
       
    Addons:
    - Es werden ggf. Infoseiten in der rechten Hälfte,
      sowie Werbungen am linken Rand eingeblendet
    - *entfernt*
    - Es werden Meldungen nur für spezielle Anzeigen eingeblendet (extragroups)
 Wegen der großen Zahl an Fallunterscheidungen ist dieses Skript
      leider eher unübersichtlich.
________________________________________________________________________

'''

# Batteries
import os
import cgitb
cgitb.enable()
# TI
import ti_lib
try:
    import ti               # gemeinsame Funktionen und 
    import konst            # Konstanten aller Komponenten
    import messages         # liefert die Nachrichten
    import persons          # liefert den Personenruf
    import infobox          # liefert die Informationsfenster, die eingeblendet werden
    import autocontent      # liefert die quasistatischen Informationen, Uhrzeit usw.
    import motto
    import weekmessage
    import webservice
    import traceback
except Exception as e:
    ti_lib.panic(_('frames.py scheiterte'),e)
    

def pm_2_s(ti_ctx,p_string,m_string,hochkant):
    '''Diese Routine klärt, ob Personenruf und Messages in eine Spalte passen, oder auf 2 Spalten verteilt werden müssen.
    Rückgabetupel: Array mit 0-2 Spaltenstrings, Anzahl der Spalten, pm_timeout'''
    p_zahl=p_string.count('<tr>')
    if p_zahl>0:
        p_zahl+=2
    m1=[]
    m2=[]
    if p_zahl:
        m1.append(p_string+'<br>')
    spalten=[]
    m_zahl=-2.2 # da folgende Schleifen jeweils einmal durchlaufen, auch wenn es keine Inhalte gibt...
    for virt_m_tag in m_string.split('<h3>'):
        m_zahl+=1.2
        for virt_m in virt_m_tag.split('<h4>'):
            m_zahl+=1
            m_zahl+=max(len(virt_m)/60,virt_m.count('<br>'))
    virtuelle_nachrichten_zeilenzahl=int(p_zahl+m_zahl)
    ti_ctx.res.debug("virt. Zeilenanzahl",str(virtuelle_nachrichten_zeilenzahl))
    #ti_ctx.res.debug("Full Content!",p_string,m_string,"-",p_zahl)
    
    if not virtuelle_nachrichten_zeilenzahl: # gibt garnichts?
        return spalten,0,0
    virtuelle_nachrichten_zeilenzahl_grenze=ti_ctx.cnf.get_db_config_int('max_virt_mess_lines',27)
    if hochkant or (virtuelle_nachrichten_zeilenzahl<=virtuelle_nachrichten_zeilenzahl_grenze): # cool, passt alles in eine Spalte
        spalten.append(''.join(m1)+m_string)
        return spalten,1,virtuelle_nachrichten_zeilenzahl/3
    # ok, doch Arbeit - wir bauen zwei Spalteninhalte auf
    ti_ctx.res.debug('Teile Messages auf!')
    neue_virtuelle_nachrichten_zeilenzahl=p_zahl # wird dann mit jeder Meldung vergrößert, bis Grenze erreicht
    # Teile messagesstring auf
    m_zeilen=m_string.split('\n')
    mtag=[]; msplitflag=False
    mchars=0
    for line in m_zeilen:
        if msplitflag:
            m2.append(line)
        else:
            h3=line.lower().count('<h3')
            h4=line.lower().count('<h4')
            br=max(line.lower().count('<br>'),len(line)/80.0)
            if h3: #ein neuer Tag beginnt
                m1+=mtag # letzten Tag sichern
                mtag=[]  # Platz für den neuen Tag
            mtag.append(line)
            neue_virtuelle_nachrichten_zeilenzahl+=1.2*(h3+h4)+br
            #ti_ctx.res.debug(str(neue_virtuelle_nachrichten_zeilenzahl))
            if neue_virtuelle_nachrichten_zeilenzahl>virtuelle_nachrichten_zeilenzahl_grenze:
                m2.append('<div class="messages"><!-- Zweite Messagesspalte -->')
                m2+=mtag
                #m2.append('<!-- das war der angefangene Tag -->')
                msplitflag=True
                ti_ctx.res.debug('Auf zur zweiten Spalte')
    if not msplitflag:
        m1+=mtag # beinhalted schließendes /div für messages!
        m1.append('<!-- das war der angefangene Tag XX sollte nicht vorkommen! -->')
    else:
        m1.append('</div>  <!-- Ende Messages links-->')
        # kein schließendes div für rechts, da schon enthalten!
        m2.append('<br>') # Aber Abstand zur Infoseite
                
    spalten.append('\n\t'.join(m1))
    if msplitflag:
        spalten.append('\n\t'.join(m2))
    else:
        ti_ctx.res.debug('Kein Split am Ende? Sollte nicht vorkommen! Vermutlich zu viel an einem Tag - unsplittbar.')
    return spalten, len(spalten),virtuelle_nachrichten_zeilenzahl/3
# Ende von pm_2_s

def compose_frames(ti_ctx):
    ti_ctx.res.debug_timestamp("Start")
    #Flags vorbelegen:
    extrastyle      =''         # css-style, wenn extrem langer Vertretungsplan oder andere Spezialitäten
    verbergeTI      =ti_ctx.cnf.get_db_config_bool('Flag_VerbergeTI',False)         # och ja, wenn's keiner wissen soll...
    zeige_werbung   =ti_ctx.cnf.get_db_config_bool('Flag_ZeigeWerbung',False)       # True, wenn links eine Werbespalte eingeblendet werden soll
    previewparameter=""         # URL-Parameter, der im Previewmodus übertragen werden soll
    columnmode      =0          # dummy
    schedule_txt='dummy'
    schedule_navstrg='dummy'
    infostrg_left=''
    timeout=20

    # Phasen: 0=Aushang, 1=Planseite 1 usw.
    phase=0; nextphase=0
    longdelay=ti_ctx.req.get_cgi_bool("longdelay")
    if longdelay:   ti_ctx.res.debug("longdelay wurde beim CGI-Aufruf gewählt!")
    framecounter=ti_ctx.req.get_cgi_int("fc")

    #######################
    # Einlesen der Parameter
    sc_code=ti_ctx.req.get_cgi_int('sc_code',0)
    if sc_code==ti_ctx.cnf.get_db_config_int('sc_code'):
        clientgroups=ti_ctx.req.get_cgi_int('setclientgroups',0)
        anzeigenparameter=0
        forward_url=''
        sc_strg=';setclientgroups={};sc_code={}'.format(clientgroups,sc_code)
        ti.client_force_gruppen(clientgroups)
        ti_ctx.res.debug('Set Client Groups erkannt:',clientgroups)
    else:
        clientgroups,anzeigenparameter,forward_url =ti_ctx.req.client.groups() #get_client_groups()
        sc_strg=''
    ti_ctx.res.debug("Clientgruppen:",str(clientgroups)+" ap:"+str(anzeigenparameter)+" sc_c:"+str(sc_code))
    soloscreen=anzeigenparameter&1
    hochkant=(anzeigenparameter&16)>0 # 1024>>6
    ti_ctx.res.debug("solo, hochkant",'{}, {}'.format(soloscreen,hochkant))
    to_be_forwarded=anzeigenparameter&4
    if to_be_forwarded:
        ti_ctx.print_html_head(timeout=0,url=forward_url if forward_url else 'ti_tool.py')
        ti.prt(_('Weiterleitung...'))
        ti_ctx.print_html_tail()
        exit()

    paralleltask1=False
    paralleltask2=False
    exturl=''
    exttaburl=''
    if clientgroups&3: # werden für Q11/Q12 weitere Meldungen importiert
        exturl=ti_ctx.cnf.get_db_config("ExtURLQ")
        eutl=exturl.lower().strip()
        if not (eutl.startswith('http://') or eutl.startswith('https://') or eutl.startswith('ftp://') ):
            exturl=''
    if clientgroups&32: # wird für Lehrer eine andere Info-Anzeige importiert
        exttaburl=ti_ctx.cnf.get_db_config("ExtURLT").lower().strip()
        eutl=exttaburl.lower().strip()
        if not (eutl.startswith('http://') or eutl.startswith('https://') or eutl.startswith('ftp://') ):
            exttaburl=''
        
    preview=ti_ctx.req.get_cgi_bool("preview") # Test auf "preview" für die Übersichtsseite im Management 
    if preview:
        extrastyle='<style type="text/css">body, div {overflow:auto; font-size:85%; }</style>'
        previewparameter=";preview=1"
        clientgroups=63     # Binärer Code für alle Gruppen
        
    # Test auf Infoseite(n):
    if exttaburl: # Lehrer mit Terminplan, der früher im Hintergrund geladen wurde
        is_info2show,infostrg=True,'wird noch geladen'
    else:
        is_info2show,infostrg=infobox.get_info(ti_ctx,mini=soloscreen) # wenn nicht is_info2show, dann kann immer noch ein Logo gezeigt werden

    #checken, ob lokales js eingebunden werden soll
    if os.path.isfile(ti.get_ti_dynpath()+'../local/local.js'):
        local_js='\n\t<script src="../local/local.js" type="text/javascript"></script>\n'
    else:
        local_js=''
    #alle Meldungen holen
    personsstring=persons.iterate_persons(ti_ctx,clientgroups & 3)
    messagesstring=messages.create_messages(ti_ctx,False)+weekmessage.get_messages(ti_ctx)
    pm_spalten,pm_spaltenzahl,pm_timeout=pm_2_s(ti_ctx,personsstring,messagesstring,hochkant)
    ti_ctx.res.debug("pm_spaltenzahl",pm_spaltenzahl)

    ti_ctx.res.debug("is_info2show",is_info2show)

    Flag_ZeigeInfos =ti_ctx.cnf.get_db_config_bool("Flag_ShowInfos",False) # liest den konfigurierten Wunsch aus
    if not Flag_ZeigeInfos:
        is_info2show=False
        infostrg=''
    ti_ctx.res.debug("Flag_ShowInfos",Flag_ZeigeInfos)
    ti_ctx.res.debug("is_info2show",is_info2show)

    infotime    =ti_ctx.cnf.get_db_config_int("InfoTime",10)
    minphase,maxphase  =ti_ctx.get_client_phaselimits()  
    ti_ctx.res.debug("Vom Admin konfigurierte phasenlimits: ",str(minphase)+' bis '+str(maxphase))
    if (minphase>=0):
        allphases=list(range(minphase,maxphase+1))
    else: # ein negatives minphase signalisiert die Phase-Menge ohne das Intervall
        allphases=list(range(0,abs(minphase)))+list(range(abs(maxphase+1),10))
    ti_ctx.res.debug("Vom Admin konfigurierte allphases: ",str(allphases))
    # allphases sind alle bisher erlaubten phase-Kandidaten, von denen nun gestrichen wird
    #########################################
    # Nun einige Entscheidungen

    # ohne Aushang fliegt dieser aus der Liste - wenn er drin steht
    if pm_spaltenzahl==0 and allphases[0]==0:
        allphases.pop(0)

    # Für konfigurierte Anzeigen:   Ist Plan 1 in der Liste, so wird morgens alles danach eliminiert
    #                               Zweck ist der Fokus auf sofort benötigte Informationen!
    if ti_ctx.req.client.istanzg and (1 in allphases) and (ti.vergleiche_mit_uhrzeit( ti_ctx.cnf.get_db_config('s_any_nur_erster_plan_bis_uhrzeit') ) <=0):
        allphases=allphases[0:allphases.index(1)+1]
        ti_ctx.res.debug('wg. Grenze nur_erster_plan_bis_uhrzeit gibt es nur einen Plan für diese Anzeige!')

    phase=ti_ctx.req.get_cgi_int("phase",1)
    ti_ctx.res.debug("phase aus URL-Parameter:",str(phase))
    if soloscreen:
        maxphase=1
        phase=1
        ti_ctx.res.debug('phase UND maxphase=1, da soloscreen')

    if not phase in allphases:
        phase=allphases[0]
        ti_ctx.res.debug('phase=allphases[0]='+str(phase)+' da phase nicht in allphases')
    # die folgende Logik, um infoseiten anzuzeigen, wird erst einmal ignoriert 2018-02-24
    #if phase>maxphase and not longdelay: # limits werden bei direktem Klick ignoriert
    #    if ( pm_spaltenzahl==0 and not is_info2show): 
    #        phase=max(minphase,1) # mindestens erste Planseite
    #        ti_ctx.res.debug('phase='+str(phase)+' da phase>maxphase und keine Aushangseite')
    #    else:
    #        phase=minphase # ggf. mindestens Aushang
    #        ti_ctx.res.debug('phase=minphase='+str(minphase)+' da phase>maxphase')
            
      
    ########################################
    # Aufruf der Vertretungsplansoftware zur Analyse
    #phase,nextphase,sched_cm=ti.analyse_schedule(phase, allphases,hochkant)
    phase,nextphase,sched_cm=ti_ctx.s_interface.analyse_schedule(phase, allphases,hochkant)
    ti_ctx.res.debug("Vertretungsplananalyse: phase",phase)
    ti_ctx.res.debug("Vertretungsplananalyse: nextphase",nextphase)
    if phase<allphases[0] and not longdelay: # limits werden bei direktem Klick ignoriert
        ti_ctx.res.debug("trotz Vertretungsplananalyse: phase ist kleiner allphases[0]:",allphases[0])
        phase,nextphase,sched_cm=ti_ctx.s_interface.analyse_schedule(allphases[0], allphases,hochkant)
    ########################################
    # Festlegung, was angezeigt wird
    if phase<0: #kein schedule verfügbar
        ti_ctx.res.debug('Festlegung: Kein Schedule verfügbar')
        phase=0
        
        
    elif phase==0 and ( ( pm_spalten==0 and not is_info2show) or allphases[0]>0): # keinerlei Meldungen, Personenrufe und Infos
        if not allphases[0]:
            ti_ctx.res.debug('Festlegung: Aushang angefordert - es gibt aber keinen - daher phase>0')
        else:
            ti_ctx.res.debug('Es soll kein Aushang angezeigt werden')
        phase=max(allphases[0],1)
                
    ########################################### 
    #Vertretungsplan eines Tages holen, denn jetzt liegt phase fest vor
    schedule_txt,extrastyle,columnmode,schedule_navstrg,timeout=ti_ctx.s_interface.compose_schedule(phase, allphases, extrastyle,sc_strg,hochkant)
    ti_ctx.res.debug("Vertretungsplancompose: columnmode",columnmode)
    if not columnmode: phase=0 # ist ja nichts anzuzeigen BieC 2020-06-12
    #############################################
    # Anzeigedauer berechnen - nach Ergebnissen
    # ggf. nextphase anpassen
    if phase>=allphases[-1]:   
        nextphase=allphases[0]         #Damit's danach direkt wieder neu losgehen kann
    if not phase:
        timeout=infotime+int(pm_timeout)
                
    if clientgroups&3:
        timeout+=10
    if clientgroups&12:
        timeout+=5
            
    if soloscreen or \
       ((phase==1 and nextphase==0 and pm_spaltenzahl<2) and columnmode==1):
        pm_muenze=(framecounter%2)==0 and pm_spaltenzahl
        nextphase=1
        if pm_muenze:
            timeout+=int(pm_timeout)
        else:
            timeout+=infotime
        ti_ctx.res.debug("Keine nextphase=0 sondern 1, da Aushang neben Plan passt! Diesmal: {}".format("Aushang" if pm_muenze else "Infoseite"))   
    else:
        pm_muenze=0
        
    #############################################
    # Anzeigedauer berechnen - höhere Mächte
    if longdelay and timeout<123:
        timeout=123
    elif soloscreen:
        timeout=max(60,min(timeout,konst.TI_MAXTIME))
    else:
        timeout=max(10,min(timeout,konst.TI_MAXTIME)) # gewisse Grenzen einhalten
    ti_ctx.res.debug("timeout",str(timeout))
        
    ################################
    # Nun die eigentliche Ausgabe: #
    ################################

    #################
    #header schreiben

    ti_ctx.print_html_head( url="/cgi-bin/frames.py"+'?phase='+str(nextphase)+previewparameter+';fc='+str((framecounter+1)%10)+sc_strg, \
                    extrastyle=extrastyle, \
                    timeout=str(timeout), \
                    onload='ti_start()', \
                    title=_("tabula.info - Bitte Vollbildschirm wählen"))
                    
    ###################################################
    # Erst die Werbung, der Rest landet im Restschirm
    if zeige_werbung and not columnmode>2:
        ti.prt('''\n<!-- werbebeginn -->
        
        <div id="wirkung" style="background-color:{bgc};">
          <table style="border:0;margin:0;padding:0;background-color:{bgc};height:100%">
           
           <tr><td style="vertical-align:middle;text-align:center;background-color:{bgc};border:0;margin:0;padding:0;">
            <img src="/dyn/bungwer.gif?rnr={rnr}" 
                 style="max-width: 100%; max-heigth: 100%; height: auto; width: auto\9; " />
           </td></tr>
           
           </table>
          
        </div> <!-- Ende Werbung-->'''.format(bgc=ti_ctx.cnf.get_db_config("WerbungHGFarbe",'#000'),rnr=ti_ctx.get_ti_lastupload()))
        #ti.prt('    </div>\n<!-- werbeende -->\n') ?
        ti.prt('    <div id="inhalt">     <!-- inhaltsbeginn -->\n')

    ##################################################
    #Navigationsleiste  & weiterer Content
    if soloscreen:
        autotime,autobase_l,autobase_r='','',''
        rechtervorhang='''
        <div id="rechtervorhang">
        {}
        <div style="height:5px;font-size=1px;">&nbsp;</div>
        {}
        <div style="height:5px;font-size=1px;">&nbsp;</div>
        {}
        </div>'''.format(personsstring,messagesstring,infostrg)
        ti.prt('<div id="solocontent">')
    else:
        autotime,autobase_l,autobase_r=autocontent.get_autocontent(ti_ctx,("00"+str(timeout))[-3:],hochkant)
        rechtervorhang=''
        darstellung='2' if ti_ctx.cnf.get_db_config_bool('Flag_Navi_unten') else ''
        navstrg='\n<div id="main_navigation{}"><div class="navi-div-links">'.format(darstellung)
        if not phase:
            titistyle=' class="main_navigation{}_selected" '.format(darstellung)
        else:
            titistyle=' class="main_navigation{}_hide" '.format(darstellung) if not phase in allphases else '' 
        tititle=ti_ctx.cnf.get_db_config('TI_Title').strip()
        navstrg+= '<a href="/cgi-bin/frames.py?phase=0;longdelay=1{}"> <span {}>{}</span>&nbsp;</a></div>'.format(sc_strg,titistyle,tititle)
        navstrg+= '<div class="navi-div-rechts">&nbsp;'
        if schedule_navstrg!="no schedule" :
            if schedule_navstrg.startswith("keine"):
                navstrg+='<small>{}</small>'.format(_('(keine Pläne)'))
            else:
                navstrg+= "&nbsp;{}&nbsp;{}".format(_('Pläne:'),schedule_navstrg)
        navstrg += '</div>'
        ti.prt(navstrg)
        ti.prt(autotime)
        ti.prt('\n</div> <!-- Ende Navigation -->\n<div id="main_content{}">'.format(darstellung))

        


    # Extra-Meldungsbereiche und Termintabelle vorbereiten
    q11=''
    q12=''
    termintabelle=''
    if exturl:
        q11=ti_ctx.cnf.get_db_config("q11_data")
        q12=ti_ctx.cnf.get_db_config("q12_data")
    if exttaburl:
        infostrg='''
        <iframe src="{}" width="99%" height="1000px" name="info_in_messages" frameborder="0" style="overflow:hidden">
            {}!
        </iframe>'''.format(exttaburl,_('Browser kann keine eingebetteten Frames anzeigen'))
        ti_ctx.res.debug('exttaburl war das')

    ####################################################################
    #Schedule schreiben
    if phase:
        ti.prt(schedule_txt)
        if columnmode==1 and not hochkant:
            ti.prt('<div id="column2_2">')
            if soloscreen or (phase==1 and nextphase!=2 and pm_spaltenzahl<2):
                if pm_spaltenzahl and (soloscreen or pm_muenze):
                    ti.prt(pm_spalten[0])
                if soloscreen or not pm_muenze:
                    ti.prt(infostrg)
                rechtervorhang=''
                # nextphase=1 ist mit gleichen Bedingungen bereits weiter oben gesetzt
            elif not Flag_ZeigeInfos and pm_spaltenzahl:
                ti.prt(pm_spalten[0])
            else:
                ti.prt(infostrg)
            ti.prt('</div> <!-- Ende Schedule C1 -->')
 
    ####################################################################
    # allgemeine Meldungen und Personenruf schreiben
    if not phase: # nullte Phase
        if is_info2show and infostrg and pm_spaltenzahl<=0: # wenn links garnix anzuzeigen ist, rechts aber eine Infoseite kommt, dann zwei Versuche für links eine andere Infoseite zu bekommen...
            ti_ctx.res.debug('Versuche linke Infoseite zu bekommen, da nix los...')
            dummy,infostrg_left=infobox.get_info(ti_ctx)
            if infostrg_left==infostrg:
                ti_ctx.res.debug('Versuche nochmal linke Infoseite zu bekommen, da gleiche wie rechts')
                dummy,infostrg_left=infobox.get_info(ti_ctx)
                if infostrg_left==infostrg:
                    ti_ctx.res.debug('Versuche linke Infoseite zu bekommen gescheitert...')
                    infostrg_left=''
                
        if hochkant:
            ti.prt('''
            <div id="column1_1" class="scrollcontent">
                <div style="overflow: visible">
                {}
                {}
                </div>
            </div> <!-- Ende Spalte 1-->\n'''.format(pm_spalten[0] if pm_spaltenzahl else ('<span style="color:grey;">'+_('Keine Meldungen anzuzeigen')+'</span>'), infostrg))
        
        else:
            ti.prt('''
            <div id="column1_2" class="scrollcontent">
                <div style="overflow: visible">
                {}
            </div></div> <!-- Ende Spalte 1-->\n'''.format(pm_spalten[0] if pm_spaltenzahl else (infostrg_left if infostrg_left else ('<span style="color:grey;">'+_('Keine Meldungen anzuzeigen')+'</span>'))))         
            if pm_spaltenzahl>1 or infostrg:
                ti.prt('''
            <div id="column2_2" class="scrollcontent">
                <div style="overflow: visible">
                {} <!-- ggf. Ende Messages -->
                {} <!-- ggf. Ende Info -->
            </div></div> <!-- Ende Spalte 2-->\n\n'''.format(pm_spalten[1] if pm_spaltenzahl>1 else '', infostrg))
        
        


        
    ###########################
    ## linkes Infofeld schreiben
    leftbasetext=[]

    # zuerst Messages mit Bits "1" "4" und "16" gesetzt
    for cg in [1,4,16]:
        if clientgroups & cg:
            leftbasetext.append( messages.create_messages(ti_ctx,cg, (q11 if cg==1 else "") ) )


    # Raumbelegungsplan
    if (ti_ctx.req.get_cgi_bool("raumbelegung")) or soloscreen:
        ti_ctx.res.debug('Zeige externes HTML!')
        exthtml=ti_ctx.cnf.get_db_config('ExterneHTMLDatei')
        if exthtml:
            dummy,infostrg2=infobox.get_info(ti_ctx, \
                infourl=exthtml+"?stunde="+str(ti.get_akt_hour()), title="")
            leftbasetext.append(infostrg2)
            
    if autobase_l: leftbasetext.append(autobase_l)

    if leftbasetext:
        ti.prt('<div id="leftbase">' + "\n".join(leftbasetext)+'</div>') ## ganzes linkes Infofeld

    ##########################
    ## Branding
    if not verbergeTI:
        ti.prt(autocontent.get_branding())

    ###########################
    ## rechtes Infofeld schreiben
    # Extrameldungen für Oberstufe, Lehrerzimmer, Motto

    mcm2=''
    rightbasetext=[]
    # zuerst Messages mit Bits ... gesetzt
    for cg in [2,8,32]:
        if clientgroups & cg:
            rightbasetext.append(messages.create_messages(ti_ctx,cg, (q12 if cg==2 else "") ) )

    if (not clientgroups&3) or preview:
        rightbasetext.append(motto.get_motto(ti_ctx))

    if autobase_r: rightbasetext.append(autobase_r)

    if rightbasetext:
        ti.prt('<div id="rightbase">' + "\n".join(rightbasetext)+'</div><!-- Ende Rightbase -->') ## ganzes rechtes Infofeld
        
    if rechtervorhang:
        ti.prt(rechtervorhang)

    #den Content abschließen
    ti.prt('</div><!-- Ende Content -->')
    #ggf. den Gesamtrahmen abschließen
    if zeige_werbung and not columnmode>2:
        ti.prt('</div>  <!-- inhaltsende (gegenüber Werbung) -->')
    if local_js:
        ti.prt(local_js)


if __name__ == '__main__':
    try:
        ti_ctx=ti.master_init()
        compose_frames(ti_ctx)
    except Exception as e:
        ti_lib.panic(_('frames.py scheiterte'),e)

    #except Exception as e:
    #    ti_lib.log('FRAME scheiterte mit '+ti_lib.beschreibe_exception(e))
    #    ti_ctx.print_html_head( title="tabula.info - interner Fehler")
    #    ti.prt('<div style="background:pink"><h2>interner Fehler</h2><pre>\n'+ti_lib.beschreibe_exception(e)+'</pre></div>')
    #html-Seite beenden
    ti_ctx.res.debug_timestamp("frames.py beendet")
    ti_ctx.print_html_tail()

# & tschüß
