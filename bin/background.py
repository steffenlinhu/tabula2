#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
________________________________________________________________________
 background.py                                                            
 EN: This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 background.py läuft im Hintergrund, stellt sicher, dass es alleine läuft
  und erledigt Aufgaben, die es in der Datenbank findet.
  Dies ist immer ein eigener Python-Task, der per subprocess startet und
  der nicht von den anderen TI-Modulen direkt aufgerufen wird!
________________________________________________________________________
'''

# Batteries
import os,time
# TI
import ti_lib, config_db


def do_periodic(ti_cnf,now):
    # normale ti-funktion
    import s_any_download
    # Schritt 1
    # daily job
    background_last=ti_cnf.get_db_config_int('background_last_epoche',0)
    if background_last and ti_lib.epoche_2_isodatum(background_last) != ti_lib.epoche_2_isodatum(now):
        ti_lib.reset_log()
        s_any_download.loesche_alle_uploads(nurheute=True)
        ti_cnf.set_db_config('s_any_force_convert',1)
    # Schritt 2
    erfolg,r=s_any_download.do_any_download(ti_cnf)
    # Schritt 3
    try:
        import s_analyzer
        s_analyzer.do_analyze(ti_cnf)
    except Exception as e:
        ti_lib.log('BKG: do_analyze scheiterte mit '+ti_lib.beschreibe_exception(e))
        
    try:
        exturl=ti_cnf.get_db_config("ExtURLQ") # absenzenmeldungen für Q11 und Q12
        eutl=exturl.lower().strip()
        if not (eutl.startswith('http://') or eutl.startswith('https://') or eutl.startswith('ftp://') ):
            exturl=''
        if exturl:
            import webservice
            q11,q12=webservice.hole_externe_daten(exturl)
            ti_cnf.set_db_config("q11_data",q11)
            ti_cnf.set_db_config("q12_data",q12)
            #ti_lib.log('BKG: webservice lädt '+q11+q12+"/n")
            ti_lib.log('BKG: webservice lädt ',len(q11)+len(q12),' Bytes')
            
    except Exception as e:
        ti_lib.log('BKG: webserviceaufruf scheiterte mit '+ti_lib.beschreibe_exception(e))

    ti_cnf.set_db_config('background_last_epoche',str(now))
        
def do_background(ti_cnf):
    """wird vom Betriebssystem als Hintergrundtask aufgerufen"""
    myid='ungesetzt'
    try:
        now=ti_lib.get_now()
        myid='{}_{}'.format(int(now*10),os.getpid())
        basepath=ti_lib.get_ti_basepath()
        logpath=ti_lib.get_ti_datapath()+'log/'
        ti_lib.log(f'BKG: {myid} als externer Background-Prozess gestartet.') 
        old_sema=ti_cnf.get_db_config('background_semaphore')
        if old_sema!="free" and old_sema<str(now-600): # Notfall. nach 10 minuten ist eine Reservierung verfallen - greift auch, wenn die Semaphore nicht gesetzt ist!
            ti_cnf.set_db_config('background_semaphore','free',old_sema)
            ti_lib.log('BKG: background_semaphore zurückgesetzt wg. zu alter Reservierung')
            
        # nun setzen wir die Semaphore mit unserer ID - wenn der Wert noch "free" ist (macht SQLITE in einem Befehl)
        ti_cnf.set_db_config('background_semaphore',myid,'free')
        # geklappt?
        if ti_cnf.get_db_config('background_semaphore')==myid:
            ti_lib.log("BKG: {} übernimmt den Job!".format(myid))
            # gut, erst einmal ein neues timeout setzen
            if ti_lib.get_akt_hour()==ti_cnf.get_db_config_int('s_any_hintergrund_rushhour',7):
                newtimeout=ti_cnf.get_db_config_int('s_any_hintergrundtakt_rushhour_s',120)
                ti_lib.log('BKG: ',ti_lib.get_akt_hour(),'Uhr -> Rushhour:',newtimeout,'s bis zum nächsten Check')
            else:
                newtimeout=ti_cnf.get_db_config_int('s_any_hintergrundtakt_s',300)
            ti_cnf.set_db_config('background_timeout',str(int( now + newtimeout))) # setzt den Timeout, wann die Webaktivitäten einen Backgroundtask starten sollen. Nicht den cron-Takt
            do_periodic(ti_cnf,now)
            ti_cnf.set_db_config('background_semaphore','free')
            ti_lib.log(f'BKG: {myid} ist erfolgreich abgeschlossen!')
        else:
            ti_lib.log(f'BKG: {myid} ist nicht dran, anderer Prozess läuft bereits lt. Semaphore')
        
    except Exception as e:
        ti_lib.log(f'BKG: background() scheiterte! myid:{myid}, Pfade:base{basepath} log{logpath}\n')
        ti_lib.log('BKG: -> Grund:',ti_lib.beschreibe_exception(e))
        


if __name__ == '__main__':
    print('BKG: Init externer Aufruf')

    import locale
    #loc = locale.getlocale(); print(loc)
    locale.setlocale(locale.LC_ALL, ('de_DE',"utf-8"))

    ti_cnf=config_db.TI_Config()
    do_background(ti_cnf)
    
