#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
________________________________________________________________________
 s_turbo_def.py                                                            
 This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 Diese Datei ist Teil von tabula.info, das Freie Software gemäß          
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 s_turbo_def.py definiert die Klassen, die beim Verwalten des
  Vertretungsplans eingelesen vom Programm "Turbo Vertretung" benötigt werden. 
 Die entsprechenden Objekte werden in import_turbo.py angelegt
  und in schedule.py angezeigt
________________________________________________________________________
'''
# Batteries
# (nix)
# TI
import ti, ti_lib   # sonst geht garnichts

# !! diese Datei wird nicht internationalisiert, das kga-spezifisch !!


'''Definition der Datenstruktur
 
 Um (mindestens) zwei Varianten bei gleicher Datengrundlage zu produzieren
 wird die Sichtbarkeit und Darstellung der Spalten über Flags gesteuert, 
 welche in je (mindestens) zwei Arrays abgelegt sind:
 Array 0 für normale Bildschirmdarstellung, 
 Array 1 für Veröffentlichung im Internet
 
 
'''
# Präfix für Kommentare - wenn es welche gibt
commenthead=['','Früher: ','']
def debug(*args):
    ti_lib.log("s_turbo:",*args)
    
class S_Schedule(object):
    ''' Objekt, das einen vollständigen Vertretungsplan definiert'''
    def __init__(self):
        self.days=[]            # Array mit 1-5 S_Day-Objekten  
        self.column_names=[]    # Liste mit den Spaltennamen
        self.column_display=[]  # Liste mit 2 Arrays, die für jede Spalte ein Flag haben,
                                #    ob die Spalte dargestellt werden soll
                                #   Erstes Array für normale Darstellung,
                                #   Zweites für Veröffentlichung
        self.column_bold=[]     # Liste mit 2 Arrays, die für jede Spalte ein Flag haben usw.
        self.column_small=[]    # Liste mit 2 Arrays, die für jede Spalte ein Flag haben usw.
        self.first5days=[]      # Liste mit day-objects (S_Day) für die folgenden 5 Tage
        
    def add_day(self,s_day):            self.days.append(s_day)
    
    def set_column_names(  self,cn):        self.column_names=cn
    def set_column_display(self,cn):        self.column_display=cn
    def set_column_bold(   self,cn):        self.column_bold=cn
    def set_column_small(  self,cn):        self.column_small=cn
    def get_column_names(  self,column):        return self.column_names[column]
    def get_column_display(self,level,column):  return self.column_display[level][column]

    def get_column_style(  self,level,column):
        if self.column_bold[ level][column]:
            return "bigright=true"
        if self.column_small[level][column]:
            return "smaller=true"
        return ""
        
    def analyze_days(self):
        '''Selbstanalyse, 
            erst aufzurufen, wenn komplett gefüllt - 
            aber frisch vor der Darstellung aufrufen 
            (wg. des Verkleinerns abgelaufener Stunden)'''
        for day in self.days:
            day.analyze_rows()
        loop_day=[]
        loop_txt=[]
        self.first5days=[]
        daydelta=0
        for i in range(30):                                 # 30 Tage durchlaufen
            iday=ti.get_following_workdayofepoche(i)        # Datum des jetzt+i-ten Werktages berechnen
            found=False
            for day in self.days:                           #alle Tage im S_Obj abklappern
                if day.get_date()==iday:                    #wenn er das oben berechnete Datum hat
                    found=True
                    the_day=day # nur eine referenz
            if found:
                self.first5days.append(the_day)
                if debuglevel>0: debug("zu first5days addiert:",the_day.get_date())
                if i>1:
                    self.first5days[-1].mark_as_plan()
                if len(self.first5days)==5:
                    break
            
    def get_first5days(self):
        return self.first5days

class S_Day(object):            # Definition eines Tages im Vertretungsplan
    ''' Objekt, das einen Tag im Vertretungsplan definiert'''
    def __init__(self,tidate):
        self.date=tidate        # tidate, für den dieses Objekt gilt
        self.datestring=ti.get_string_from_dayofepoche(tidate,short=True)
        self.datestringshort=ti.get_string_from_dayofepoche(tidate,veryshort=True)
        self.rows=[]            # Liste mit Zeilenobjekten vom Typ S_Row
        self.comment=['','',''] # Drei Kommentartexte für den ganzen Tag
        if debuglevel>0: debug("New S_Day:",str(tidate))
        
    def add_row(self,s_row,deduplicatecell=-1):
        appendflag=False
        noappend=False
        if deduplicatecell==-1:
            appendflag=True
        else:
            for row in self.rows:
                if not noappend and abs(s_row.expire-row.expire)==1:
                    testflag=True
                    for i in range(len(row.cells)):
                        if i!=deduplicatecell and row.cells[i]!=s_row.cells[i]:
                            testflag=False
                    if testflag:
                        row.cells[deduplicatecell]=row.cells[deduplicatecell]+"+"+s_row.cells[deduplicatecell]
                        noappend=True
            if not noappend:
                appendflag=True
                
        if appendflag:
            self.rows.append(s_row)
        
    def add_comment(self,comment,level=0): # Kommentare in 100-102 in csv als 0-2 in level
        global debuglevel
        debug("Setze Kommentar , level",(comment,level))
        
        if level and self.comment[level]:
            self.comment[level]+=' |&nbsp;'
        self.comment[level]+=comment
        if debuglevel>1: debug("Setze Kommentar im level",(comment,level))
        
    def analyze_rows(self):
        '''Selbstanalyse. Aufruf erst wenn vollständig gefüllt!
        Errechnet Variablen, die mit get_rowmedian usw. ausgelesen werden'''
        global debuglevel
        # Gesamthöhe = 0.6 * kleinere Zeilen + volle Zeilen
        # halbe Höhe = Gesamthöhe/2
        # benötigte Zeilen für halbe Höhe:
        #  halbe Höhe/0,6 ergibt kleine Zeilen-Anzahl
        #  wenn genug kleine Zeilen -> Lösung
        #  wenn zu wenig kleine Zeilen:
        #   fehlende Höhe = halbe höhe - 0,6*vorhandene kleine zeilen
        #   zusätzliche (volle) Zeilen= fehlende höhe :-)
        # Analog für Drittel...
        akthour=ti.get_akt_hour(self.date) # liefert -1 wenn nicht "heute"
        all2display=list(range(1,16))
        self.rows.sort(key=lambda zeile: zeile.sortvalue)
        row_count=0
        small_row_count=0
        for row in self.rows:
            try:
                row_hour=row.get_expire()
                if row_hour in all2display:
                    row_count      += 1 if row_hour>akthour else 0.6
                    small_row_count+= 0 if row_hour>akthour else 1
            except:
                pass                    #ignore 
        for l_i in (0,1):
            row_count+= (len(self.comment[l_i].split('<BR>'))*4.0)/5.0
            # grobe Schätzung, dass jeder Absatz ca. 4/5-Zeilen lang ist

        median=(row_count/2)/0.52
        drittel=(row_count/3)/0.52
        zweidrittel=(row_count*2/3)/0.52
        if median > small_row_count:
            median=((row_count/2.0)-0.52*small_row_count)+small_row_count
        if drittel > small_row_count:
            drittel=((row_count/3.0)-0.52*small_row_count)+small_row_count
        if zweidrittel > small_row_count:
            zweidrittel=((row_count*2.0/3.0)-0.52*small_row_count)+small_row_count

        self.rowcount=int(row_count+0.5)
        self.rowmedian=int(median+0.5)
        self.rowthird=int(drittel+0.5)
        self.rowtwothird=int(zweidrittel+0.5)
        #if debuglevel>0: 
        if debuglevel>0: debug("Analysierte Rows für:",str(self.date)+"(="+ti.get_string_from_dayofepoche(self.date)+") C:"+str(self.rowcount)+"M:"+str(self.rowmedian))
        #if debuglevel>0: 
        if debuglevel>0: debug(" Analys.Rows: median:",str(self.rowmedian)+" Thrd:"+str(self.rowthird)+" 2Thrd:"+str(self.rowtwothird))
    
    def get_rowcount(self):     return self.rowcount
    def get_rowmedian(self):    return self.rowmedian
    def get_rowthird(self):     return self.rowthird
    def get_rowtwothird(self):  return self.rowtwothird
    def get_date(self):         return self.date
    def get_datestring(self):   return self.datestring
    def get_datestringshort(self):  return self.datestringshort
    def get_comment(self,level):      return (commenthead[level]+self.comment[level] if self.comment[level] else '')
        
    def mark_as_plan(self):
        self.datestring+=' (geplant)'

class S_Row(object):
    ''' Objekt, das eine Zeile im Vertretungsplan definiert'''
    global debuglevel
    def __init__(self,cells,expire=99,highlight=0,sortvalue=0):
        self.cells=cells        # Array mit allen Zellen der Zeile
        self.expire=int(expire)     # Letzte Unterrichtsstunde, in der diese Zeile gezeigt wird
                                #  (reduntante Information, da wohl in irgendeiner Spalte enthalten)
        self.highlight=highlight    # red, green, yellow?
                                #Definition:0: Nix, also schwarze Schrift auf weiß
                                #           1: Rote Schrift auf weiß
                                #           2: Grüne Schrift auf weiß
                                #           3: Gelber Hintergrund mit schwarzer Schrift
                                #           4: Grüner Hintergrund mit schwarzer Schrift
        if debuglevel>1: debug("New S_Row:",str(expire)+">>"+str(cells))
        self.sortvalue=sortvalue if sortvalue>0 else expire
        if debuglevel>1: debug("New Sortvalue:",self.sortvalue)
        
    def get_cells(self): 
        return self.cells
    def get_expire(self):
        return self.expire
    def get_highlight(self):
        return self.highlight

debuglevel=1
if __name__ == '__main__':
    ti_ctx=ti.master_init()
    ti_ctx.print_html_head("def_schedule.py")
    ti.prt("<H3>Diese Datei hat keinen Selbsttest</H3>")
    ti_ctx.print_html_tail()
