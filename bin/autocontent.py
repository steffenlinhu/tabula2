#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
________________________________________________________________________
 autocontent.py                                                            
 This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 Diese Datei ist Teil von tabula.info, das Freie Software gemäß          
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 autocontent.py liefert quasi-statische Informationen wie Uhrzeit, 
 Eigenwerbung und interne Meldungen
________________________________________________________________________
'''
# Batteries
import random
# TI
import ti
import konst

def get_branding():
    mybranding=konst.TI_BRANDING
    branding= '''
    <div id="branding">
            <p>{branding}</p>
    </div>'''.format( branding = mybranding)
    return branding

def get_autocontent(ti_ctx,countdown,hochkant=False):
    # contenttime: divs sind definiert mit float:right und werden daher in umgekehrter Reihenfolge dargestellt
    if not hochkant:
        atime=ti.get_string_from_epoche(pre="Es ist ",middle= ", der ",post=" Uhr.")
    else:
        atime=ti.get_string_from_epoche(middle="",dtsep=" - ")
    contenttime= '''
        <div class="navi-div-cd" id="ti_time">{countdown}s</div>
        <div class="navi-div-zeit"           >{time}</div>
        '''.format(time=atime, \
                countdown=countdown, \
                typ="l" if ti_ctx.cnf.get_db_config_bool("Flag_HellesThema") else "c", \
                random=random.random())
    st_l=ti_ctx.cnf.get_db_config("SubTextL","").strip()
    st_r=ti_ctx.cnf.get_db_config("SubTextR","").strip()
    if len(st_l):
        base_l= '\n<div class="firstcontent">{internalinfo}</div>\n'.format(internalinfo=st_l)
    else: base_l=""
    if len(st_r):
        base_r= '\n<div class="firstcontent">{internalinfo}</div>\n'.format(internalinfo=st_r)
    else: base_r=""
    return contenttime, base_l, base_r

def driveautocontent(ti_ctx):
    ti_ctx.print_html_head("autocontent.py SELFTEST")
    for c in get_autocontent(ti_ctx,20):
        ti.prt(c)
    ti.prt(get_branding())
    ti_ctx.print_html_tail()
    

if __name__ == '__main__':
    ti_ctx=ti.master_init()
    driveautocontent(ti_ctx)
