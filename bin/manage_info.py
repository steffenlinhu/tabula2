#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
________________________________________________________________________
 manage_info.py                                                            
 This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 Diese Datei ist Teil von tabula.info, das Freie Software gemäß
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 manage_info.py ermöglicht informationen als PDF oder Bild hochzuladen
________________________________________________________________________
'''

# Batteries
import cgi
import cgitb; cgitb.enable()
import os, sys
# TI
import ti_lib
import ti
import konst

def print_html_form(ti_ctx,spezialrechte=0):
    """This prints out the html form. Note that the form is a self-posting form.
    In other words, this cgi both displays a form and processes it.
    """
    HTML_TEMPLATE = '''
    <div class="firstcontent" >
        <h1>{}</h1>
    </div>
    <div class="infos"  style="margin-top:0.5em">

        {{imageliste}}
        
        {{spezialliste}}
        
    </div>
    '''.format(_('Info-Seiten aktualisieren'))

    dynpath=ti.get_ti_dynpath()
    rnr=ti_ctx.get_ti_lastupload() # random number - nicht wirklich...
    maxinfopix=min(ti_ctx.cnf.get_db_config_int('MaxInfoPix',6),99) # 99, are U nuts?
    #imagelist='<div style="background-color:white">Diese Seiten werden im zufälligen Wechsel angezeigt:<br><table style="background-color:white"><tr>'
    imagelist='<div>{}:<br><table ><tr>'.format(_('Diese Info-Seiten werden im zufälligen Wechsel angezeigt'))
    zaehler=0
    for i in range(maxinfopix):
        filename ='info'+str(i)+'.png'
        thumbnail='info'+str(i)+'small.png'
        if os.path.isfile(dynpath+filename): # and os.path.isfile(basepath+thumbnail):
            if zaehler and not zaehler%5:
                imagelist+='</tr>\n<tr>'
            info_by=ti_ctx.cnf.get_db_config('info_by_'+filename,'-')
            imagelist+='<td style="padding:0.5em;text-align:center;not-background-color:white"><a href="/'+'dyn/'+filename+'?rnr='+rnr+'" target="_blank"><img src="/'+'dyn/'+thumbnail+'?rnr='+rnr+'"></a>'
            imagelist+='<br><span style="text-align:right">{}'.format(_('Infoseite hochgeladen von'))+' '+info_by+'</span>'
            imagelist+='<a href="manage_info.py?number_2_delete='+str(i)+'"><br><img src="/static/erase.png" width="150" height="19" border="0" alt="delete"></a></td>\n'
            zaehler+=1
            
                
    if zaehler>0:
        imagelist+='</tr></table><br></div>'
    else:
        imagelist=""
    if zaehler>=maxinfopix:
        imagelist+='<div style="background-color:orange">{}</div>'.format(_('Meldung: Es können keine weiteren Bilder hinzugefügt werden!'))
    else:
        if zaehler>(maxinfopix/2):
            imagelist+='<div style="background-color:yellow">{}</div>'.format(_('Warnung: Da so viele Bilder angezeigt werden sollen, sieht man die einzelnen Bilder recht selten!'))
        imagelist+='''
                <form action="manage_info.py" method="POST" enctype="multipart/form-data">
                    <div style="not-background-color:white"><br>''' + \
                        _('''Neue Infoseite hochladen (PDF/A und Bildformate)<br>
                        - Transparente Bereiche werden weiß<br>
                        - weisse Ränder werden abgeschnitten<br>
                        - Rest wird in 950x1000 Pixel seitenrichtig eingepasst''')+'<br>'+ \
                        _('Datei wählen')+''': <input name="info_file" type="file" /><br>
                        <input type="submit" name="hochladen" value=" '''+_('Hochladen')+''' " /><br><br>
                    </div>
                </form>'''
    spezialliste=""
    zaehler=0
    if spezialrechte:
        spezialliste='''<div style="not-background-color:white">
                            <table style="not-background-color:white;border:0">
                                <tr>
                                    <th colspan=2>'''+_('Die folgenden Bilder werden mit höherer Priorität gezeigt')+''':</th>
                                    <th style="background-color:blue;color:white;border:0"> '''+_('Info für Lehrkräfte')+''':</th>
                                    <th style="background-color:grey;border:0"> '''+_('Werbespalte')+''' :</th></tr>
                                <tr>'''
        for i in range(2):
            filename ='info'+str(i)+'.gif'
            thumbnail='info'+str(i)+'small.gif'
            if os.path.isfile(dynpath+filename): # and os.path.isfile(basepath+thumbnail):
                info_by=ti_ctx.cnf.get_db_config('info_by_'+filename,'-')
                spezialliste+='<td style="padding:0.5em;text-align:center;not-background-color:white;border:0"><a href="/'+'dyn/'+filename+'?rnr='+rnr+'" target="_blank"><img src="/'+'dyn/'+thumbnail+'?rnr='+rnr+'"></a>'
                spezialliste+='<br><span style="text-align:right">'+_('Infoseite hochgeladen von')+' '+info_by+'</span>'
                spezialliste+='<a href="manage_info.py?number_2_delete='+str(100+i)+'"><br><img src="/static/erase.png" width="150" height="19" border="0" alt="loeschen"></a></td>\n'               
                zaehler+=1
            else:
                spezialliste+='<td style="not-background-color:white;border:0"></td>'
        filename ='lehrer.gif'
        thumbnail='lehrersmall.gif'
        if os.path.isfile(dynpath+filename): # and os.path.isfile(basepath+thumbnail):
            info_by=ti_ctx.cnf.get_db_config('info_by_'+filename,'-')
            spezialliste+='<td style="padding:0.5em;text-align:center;background-color:blue;border:0"><a href="/'+'dyn/'+filename+'?rnr='+rnr+'" target="_blank"><img src="/'+'dyn/'+thumbnail+'?rnr='+rnr+'"></a>'
            spezialliste+='<br><span style="text-align:right">'+_('Infoseite hochgeladen von')+' '+info_by+'</span>'
            spezialliste+='<a href="manage_info.py?number_2_delete='+str(200)+'"><br><img src="/static/erase.png" width="150" height="19" border="0" alt="loeschen"></a></td>\n'
        else:
            spezialliste+='<td style="background-color:blue;border:0">&nbsp;</td>\n'
        filename ='bungwer.gif'
        thumbnail='bungwersmall.gif'
        if os.path.isfile(dynpath+filename): # and os.path.isfile(basepath+thumbnail):
            info_by=ti_ctx.cnf.get_db_config('info_by_'+filename,'-')
            spezialliste+='<td style="padding:0.5em;text-align:center;background-color:grey;border:0"><a href="/'+'dyn/'+filename+'?rnr='+rnr+'" target="_blank"><img src="/'+'dyn/'+thumbnail+'?rnr='+rnr+'"></a>'
            spezialliste+='<br><span style="text-align:right">'+_('Infoseite hochgeladen von')+' '+info_by+'</span>'
            spezialliste+='<a href="manage_info.py?number_2_delete='+str(300)+'"><br><img src="/static/erase.png" width="150" height="19" border="0" alt="loeschen"></a></td>\n'                
        spezialliste+='</tr><tr><td style="not-background-color:white;border:0" colspan=2>'
        spezialliste+='</tr><tr><td style="not-background-color:white;border:0" colspan=2>'
        if zaehler<2:
            spezialliste+='''
            <form action="manage_info.py" method="POST" enctype="multipart/form-data">
                <div style="not-background-color:white"><br>
                    '''+_('''Neues PDF/Bild hochladen. Wird als (animiertes) GIF gespeichert.<br>
                    Es wird in 950x1000 Pixel unverzerrt eingepasst.<br>
                    Transparente Bereiche werden weiß.<br>
                    Datei wählen''')+''': <input name="info_gif" type="file"></input><br>
                    <input type="submit" name="hochladen" value=" '''+_('Hochladen')+''' "></input><br><br>
                </div>
            </form>'''
        else:
            spezialliste+=_('Es können hier keine weiteren Bilder hinzugefügt werden.')
        spezialliste+='</td><td  style="not-background-color:white;border:0">'
        spezialliste+='''
            <form action="manage_info.py" method="POST" enctype="multipart/form-data">
                <div style="background-color:blue;color:white"><br>
                    '''+_('''Neues PDF/Bild für Lehrer hochladen. Wird als (animiertes) GIF gespeichert.<br>
                    Transparente Bereiche werden blau!<br>
                    Datei wählen''')+''': <input name="lehrer_gif" type="file"></input><br>
                    <input type="submit" name="hochladen" value=" '''+_('Hochladen')+''' "></input><br><br>
                </div>
            </form>'''
        spezialliste+='</td><td  style="not-background-color:white;border:0">'
        spezialliste+='''
            <form action="manage_info.py" method="POST" enctype="multipart/form-data">
                <div style="background-color:grey"><br>
                    '''+_('''Neue Werbespalte als GIF hochladen.<br>
                    Transparente Bereiche werden schwarz!<br>
                    Datei wählen''')+''': <input name="werbe_gif" type="file"></input><br>
                    <input type="submit" name="hochladen" value=" '''+_('Hochladen')+''' "></input><br><br>
                </div>
            </form>'''
        spezialliste+='</tr></table><br></div>'
        
    
    import login
    ti_ctx.print_html_head(ismanagement=True)
    ti.prt(HTML_TEMPLATE.format(    imageliste=imagelist,          \
                                    spezialliste=spezialliste ) )
    ti.prt(ti_ctx.get_client_ip_or_name()) 
    ti_ctx.print_html_tail()

def save_uploaded_file(ti_ctx):
    """This saves a file uploaded by an HTML form.
    The form_field is the name of the file input field from the form.
    If no file was uploaded or if the field does not exist then
    this does nothing.
    """
    maxinfopix=min(ti_ctx.cnf.get_db_config_int('MaxInfoPix',6),20)
    success=False
    dynpath=ti.get_ti_dynpath()
    ist_gif=False
    ist_werbung=False
    ist_lehrer=False
    if len(ti_ctx.req.get_cgi('info_file'))<2:
        if len(ti_ctx.req.get_cgi('info_gif'))>=2: 
            ist_gif=True
        elif len(ti_ctx.req.get_cgi('werbe_gif'))>=2: 
            ist_werbung=True
        elif len(ti_ctx.req.get_cgi('lehrer_gif'))>=2: 
            ist_lehrer=True
        if not (ist_gif or ist_werbung or ist_lehrer): 
                ti_ctx.res.debug("HTML-Upload","keine Dateien übergeben")   
                return ""
    resultat="-"+_('Upload fehlgeschlagen') # wird ggf. überschrieben...
    
    if ist_gif:
        fileitem = ti_ctx.req.get_cgi_object('info_gif')
    elif ist_werbung:
        fileitem = ti_ctx.req.get_cgi_object('werbe_gif')
    elif ist_lehrer:
        fileitem = ti_ctx.req.get_cgi_object('lehrer_gif')
    else:
        fileitem = ti_ctx.req.get_cgi_object('info_file')
    if not fileitem.file: return False
    ti_ctx.res.debug("HTML-Upload","lese Info-Datei "+fileitem.filename)
    fout = open(dynpath+"infoupload", 'wb')            
    chunk = fileitem.file.read(1500000)                 # limited to 1,5MB - wird ggf. wiederholt
    while chunk:
        fout.write (chunk)
        success=True
        chunk = fileitem.file.read(15000000)
    if success:
        ti_ctx.res.debug("HTML-Upload","erfolgreich geschrieben")
    else:
        ti_ctx.res.debug("HTML-Upload","doch keine erste Datei")
    fout.close()
    if success:
        import subprocess   
        meldung=""
        filetyp='gif' if ist_gif else 'png'
        if not (ist_werbung or ist_lehrer):
            for i in range(2 if ist_gif else maxinfopix):
                filename ='info'+str(i)+'.'+filetyp
                if not os.path.isfile(dynpath+filename):
                    thumbnail='info'+str(i)+'small.'+filetyp
                    try:
                        if ist_gif:
                            if fileitem.filename.lower().endswith('.pdf'):
                                info_ani_delay=' -delay {} '.format(ti_ctx.cnf.get_db_config_int('info_pdf_ani_delay',5)*100)
                            else:
                                info_ani_delay=''
                            subprocess.check_output('gm convert '+ info_ani_delay + \
                                                                dynpath+'infoupload -coalesce -bordercolor white -border 0 -resize  250x250 '+ \
                                                                dynpath+thumbnail+' &', shell=True, stderr=subprocess.STDOUT)
                            subprocess.check_output('gm convert '+ info_ani_delay + \
                                                                dynpath+'infoupload -coalesce -bordercolor white -border 0 -resize 950x1000 '+ \
                                                                dynpath+filename , shell=True, stderr=subprocess.STDOUT)
                        else:
                            subprocess.check_output('gm convert '+ dynpath+'infoupload[0] -background white -flatten -trim  -resize 950x1000  -bordercolor white -interlace line '+ \
                                                                dynpath+filename , shell=True, stderr=subprocess.STDOUT)
                            subprocess.check_output('gm convert '+ dynpath+filename+'         -resize  250x250 '+ \
                                                                dynpath+thumbnail, shell=True, stderr=subprocess.STDOUT)
                        ti_ctx.cnf.set_db_config('info_by_'+filename,ti_ctx.get_client_ip_or_name())
                    except subprocess.CalledProcessError as err:
                        meldung='<pre>'+err.output.decode("utf-8", "ignore")+'</pre>'
                    resultat=('+'+_('erfolgreicher Upload mit Konvertierung')) if not meldung else ('-'+_('Es ist ein Fehler bei der Konvertierung aufgetreten')+':<small><small><br>'+meldung)
                    break
            else:
                resultat='-'+_('Kein Slot mehr frei! Löschen Sie ein Bild vor dem nächsten Upload') # wird ggf. überschrieben...
        elif ist_werbung: # konvertiere die Werbespalte
            try:
                subprocess.check_output('gm convert '+dynpath+'infoupload -coalesce -bordercolor black -border 0 -resize 135x1000  '+dynpath+'bungwer.gif' , shell=True, stderr=subprocess.STDOUT)
                subprocess.check_output('gm convert '+dynpath+'infoupload -coalesce -bordercolor black -border 0 -resize  250x250  '+dynpath+'bungwersmall.gif', shell=True, stderr=subprocess.STDOUT)
                ti_ctx.cnf.set_db_config('info_by_bungwer.gif',ti_ctx.get_client_ip_or_name())
            except subprocess.CalledProcessError as err:
                meldung='<pre>'+err.output.decode("utf-8", "ignore")+'</pre>'
            resultat=('+'+_('erfolgreicher Upload mit Konvertierung')) if not meldung else ('-'+_('Es ist ein Fehler bei der Konvertierung aufgetreten')+':<small><small><br>'+meldung)
        elif ist_lehrer: # konvertiere die Seite für das Lehrerzimmer
            try:
                subprocess.check_output('gm convert '+dynpath+'infoupload -coalesce -bordercolor blue -border 0 -resize  250x250  '+dynpath+'lehrersmall.gif &', shell=True, stderr=subprocess.STDOUT)
                subprocess.check_output('gm convert '+dynpath+'infoupload -coalesce -bordercolor blue -border 0 -resize 950x1000  '+dynpath+'lehrer.gif' , shell=True, stderr=subprocess.STDOUT)
                ti_ctx.cnf.set_db_config('info_by_lehrer.gif',ti_ctx.get_client_ip_or_name())
            except subprocess.CalledProcessError as err:
                meldung='<pre>'+err.output.decode("utf-8", "ignore")+'</pre>'
            resultat=('+'+_('erfolgreicher Upload mit Konvertierung')) if not meldung else ('-'+_('Es ist ein Fehler bei der Konvertierung aufgetreten')+':<small><small><br>'+meldung)
    ti_ctx.set_erfolgsmeldung(resultat)             
    ti_ctx.set_ti_lastupload()
    
def dodelete(ti_ctx):
    n2d=ti_ctx.req.get_cgi_int('number_2_delete',-1)
    dynpath=ti.get_ti_dynpath()
    if n2d>=0:
        if n2d<100:
            filename ='info'+str(n2d)+'.png'
            thumbnail='info'+str(n2d)+'small.png'
        elif n2d<200:
            filename ='info'+str(n2d-100)+'.gif'
            thumbnail='info'+str(n2d-100)+'small.gif'
        elif n2d==200:
            filename ='lehrer.gif'
            thumbnail='lehrersmall.gif'
        elif n2d==300:
            filename ='bungwer.gif'
            thumbnail='bungwersmall.gif'
        if os.path.isfile(dynpath+filename):
            import subprocess
            subprocess.call('rm '+dynpath+filename , shell=True)
            error=subprocess.call('rm '+dynpath+thumbnail , shell=True)
            ti_ctx.set_erfolgsmeldung( ('+'+_('Erfolgreich gelöscht')) if not error else ('-'+_('Fehler beim Löschen')+': '+str(error)) )
        else:
            ti_ctx.set_erfolgsmeldung('-'+_('Zu löschende Datei nicht gefunden'))
    
if __name__ == '__main__':
    try:
        ti_ctx=ti.master_init()
        ti_ctx.res.debug("HTML-Upload","gestartet") 
        save_uploaded_file(ti_ctx)
        dodelete(ti_ctx)
        print_html_form(ti_ctx,ti_ctx.check_management_rights(konst.RECHTEInfoupload | konst.RECHTEInfouploaderweitert) & konst.RECHTEInfouploaderweitert)
    except Exception as e:
        ti_lib.panic("manage_info.py scheiterte",e)

