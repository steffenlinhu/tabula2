#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
________________________________________________________________________
 config_db.py                                                            
 EN: This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß          
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 config_db.py kapselt die Zugriffe auf die Konfigurationsdatenbank.
 Die Zugriffe werden von allen Modulen an ein Objekt der Klasse 
 TI_Config gerichtet, welches an geeigneter Stelle erzeugt wird.
 
 Es werden bewusst keine anderen ti-Module außer dem 
 unproblematischen ti_lib aufgerufen!
________________________________________________________________________
'''

# Batteries
import sys, traceback
import ast, codecs
import os.path, pwd
import sqlite3, time
import socket
import time 
# TI
import ti_lib

class TI_Config():
    def __init__(self):
        ## Verbindung zur Datenbank herstellen
        try:
            self.conf_db_conn=sqlite3.connect(ti_lib.__ti_datapath__+'configdb.sq3')
            self.conf_db_cursor=self.conf_db_conn.cursor()
            ## Standarddefinitionen, auf die "gebaut" werden kann - nicht mehr ändern!!
            self.conf_db_cursor.execute('CREATE TABLE IF NOT EXISTS configpairs (parameter TEXT PRIMARY KEY, wert TEXT) ')
            self.conf_db_cursor.execute('''CREATE TABLE IF NOT EXISTS clients 
                                        (   ip TEXT PRIMARY KEY, 
                                            timestamp INTEGER DEFAULT 0, 
                                            name TEXT, 
                                            gruppen INTEGER DEFAULT 0, 
                                            maxphase INTEGER DEFAULT 0,
                                            istconf INTEGER DEFAULT 0) ''')
            ## Weitere Definitionen oder Abweichungen weiter unten in dem Test der dbversion einbauen
            self.conf_db_conn.commit()

        except Exception as e:
            ti_lib.panic(_('Keine Verbindung zur Konfigurationsdatenbank configdb.sq3. Rechte falsch gesetzt?'),e,'basepath:'+ti_lib.__ti_basepath__+'datapath:'+ti_lib.__ti_datapath__+'<br>username:'+ti_lib.__username__)

        self.clientgroups=0
        self.conf_db_upgrade()
        
    def conf_db_upgrade(self):
        """Diese Routine stellt sicher, dass mit einer aktuellen Datenbank gearbeitet wird. die Überprüfung kostet quasi nichts."""
        dbversion=self.get_db_config('ticonfig_db_version','') 
        if dbversion<'0.9.8b':
            try: 
                self.conf_db_cursor.execute('''ALTER TABLE clients ADD COLUMN istanzg INTEGER DEFAULT 0''') # bewusste Erzeugung eines ersten Upgrades (real: von 0.9.8.alpha zu 0.9.8.beta) 
            except: pass
            try: 
                self.conf_db_cursor.execute('''ALTER TABLE clients ADD COLUMN cukopplung INTEGER DEFAULT 0''') # Clientname - Username - Kopplung für einfachere Rechtevergabe
            except: pass
            self.set_db_config('ticonfig_db_version','0.9.8b')
            dbversion=self.get_db_config('ticonfig_db_version','') 

        if dbversion<'0.9.9':
            try: 
                self.conf_db_cursor.execute('''ALTER TABLE clients ADD COLUMN forward TEXT DEFAULT ""''') # Erzeugung eines Upgrades (real: von 0.9.9.preview zu 0.9.9.final) 
            except: pass
            # self.conf_db_conn.commit() in set_db_config enthalten
            self.set_db_config('ticonfig_db_version','0.9.9')
            dbversion=self.get_db_config('ticonfig_db_version','') 

        if dbversion<'0.9.X7':
            self.conf_db_cursor.execute('DROP TABLE IF EXISTS s_files')
            self.conf_db_cursor.execute('''CREATE TABLE s_files (
                                                name    TEXT PRIMARY KEY,
                                                md5     TEXT,
                                                checked INTEGER )''') # neue Tabelle für s_any
            self.conf_db_cursor.execute('DROP TABLE IF EXISTS s_any_vplan')
            self.conf_db_cursor.execute('''CREATE TABLE s_any_vplan (
                                                epoche  INTEGER NOT NULL,
                                                phase   INTEGER NOT NULL,
                                                ziel    INTEGER NOT NULL,
                                                spalten INTEGER DEFAULT 1,
                                                url     TEXT,
                                                dauer   INTEGER, 
                                                weite   INTEGER DEFAULT 0,
                                                qf      INTEGER DEFAULT 0,
                                                PRIMARY KEY (epoche,phase,ziel))''') # neue Tabelle für alle s_any-Dateien

            self.set_db_config('ticonfig_db_version','0.9.X7')
            dbversion=self.get_db_config('ticonfig_db_version','') 
        if dbversion<'1.1.a4':
            self.set_db_config('flag_s_any_html_ganzseitig',self.get_db_config_bool('flag_s_any_html_zweispaltig',False))
            try: 
                self.conf_db_cursor.execute('''ALTER TABLE clients ADD COLUMN minphase INTEGER DEFAULT 0''') 
            except: pass
            self.conf_db_cursor.execute('''UPDATE clients SET maxphase=10 WHERE maxphase=0''')
            self.set_db_config('ticonfig_db_version','1.1.a4')
            dbversion=self.get_db_config('ticonfig_db_version','') 

    def get_s_any_2epochen(self):
        'Liefert die letzten beiden Epochen'
        self.conf_db_cursor.execute('SELECT DISTINCT epoche FROM s_any_vplan ORDER BY epoche DESC LIMIT 2')
        erste=0
        zweite=0
        for zeile in self.conf_db_cursor:
            zweite=erste
            erste=zeile[0]
        return erste,zweite

    def get_s_any_aktuelle_epoche(self):
        'Liefert die aktuelle Epoche'
        return self.get_db_config_int('s_any_akt_epoche',0)
        
    def set_s_any_aktuelle_epoche(self,akt_epoche):
        '''Setzt die aktuelle Epoche
         - sollte also nach allen set_s_any_vplan aufgerufen werden
         - löschte alle vor der vorletzen Epoche
         - Liefert Liste aller gelöschten epochen zurück, um ein Verzeichnislöschen zu ermöglichen'''
         
        self.set_db_config('s_any_akt_epoche',akt_epoche)
        erste,zweite=self.get_s_any_2epochen()
        delepochen=[]
        self.conf_db_cursor.execute('SELECT DISTINCT epoche FROM s_any_vplan WHERE epoche NOT IN (?,?)',(erste,zweite)) 
        for row in self.conf_db_cursor:
            delepochen.append(row[0])
        self.conf_db_cursor.execute('DELETE FROM s_any_vplan WHERE epoche NOT IN (?,?)',(erste,zweite)) 
        self.conf_db_conn.commit()
        return delepochen

        
    def get_s_any_vplan_liste(self,ziel):
        'liefert eine Liste mit S_any_vplan-Objekten für das Ziel - mit default-Dateien, wenn es für das Ziel nichts gibt'
        akt_epoche=self.get_s_any_aktuelle_epoche()
        self.conf_db_cursor.execute('SELECT phase,spalten,url,dauer,weite FROM s_any_vplan WHERE epoche=? AND ziel=? ORDER BY phase ASC',(akt_epoche,ziel))
        liste=[]
        for zeile in self.conf_db_cursor:
            neuer_s_any_vplan= ti_lib.S_any_vplan(akt_epoche,ziel,phase=zeile[0],spalten=zeile[1],url=zeile[2],dauer=zeile[3],weite=zeile[4])
            liste.append(neuer_s_any_vplan)
        if liste:
            debuginfo='Es gibt {} Pläne für das Ziel {}'.format(len(liste),ziel)
            return liste,debuginfo
        # keine Zielpläne?
        if ziel!=0:                         # wenn noch nicht default
            return self.get_s_any_vplan_liste(0) # dann defaultpläne
        debuginfo='Es gibt keine Pläne für das Ziel {} und keine defaults!'.format(len(liste),ziel)
        return [],debuginfo # sehr doof, es gibt also keine default-Pläne

    def set_s_any_vplan(self,s):
        'schreibt den db-Eintrag für eine vplan-Datei, die als S_any_vplan-Objekt übergeben wurde'
        self.conf_db_cursor.execute('''INSERT INTO s_any_vplan 
                        (epoche,ziel,phase,spalten,url,dauer,weite,qf) 
                VALUES  (?,?,?,?,?,?,?,?)''', \
                (s.epoche,s.ziel,s.phase,s.spalten,s.url,s.dauer,s.weite,s.qf))
        self.conf_db_conn.commit() #  weil sonst zu viel Zeit bis zum commit vergeht und database locked passiert!

    def s_files_get_md5(self,name):
        try:
            self.conf_db_cursor.execute('SELECT md5 FROM s_files WHERE name=?',(name,))
            for row in self.conf_db_cursor:
                return row[0]
        except:
            pass
        return ''

    def s_files_set_md5(self,name,md5):
        self.conf_db_cursor.execute('INSERT OR REPLACE INTO s_files (name,md5,checked) VALUES (?,?,1)',(name,md5))
        self.conf_db_conn.commit()

        
    def s_files_count_n_delete_unchecked(self):
        self.conf_db_cursor.execute('SELECT count(name) FROM s_files WHERE checked=0')
        for row in self.conf_db_cursor:
                anzahl=row[0]
        self.conf_db_cursor.execute('DELETE FROM s_files WHERE checked=0')
        self.conf_db_conn.commit()
        return anzahl

    def s_files_uncheck(self):
        self.conf_db_cursor.execute('UPDATE s_files SET checked=0')
        self.conf_db_conn.commit()

    def get_db_config(self,parameter,default=''):
        wert=default
        try:
            self.conf_db_cursor.execute('SELECT wert FROM configpairs WHERE parameter=?',(parameter,))
            for row in self.conf_db_cursor:
                wert=row[0].strip()
        except:
            pass
        return wert

    def get_db_config_int(self,parameter,default=-1):
        return ti_lib.strg2int(self.get_db_config(parameter),default)
        
    def get_db_config_bool(self,parameter,default=False):
        return ti_lib.strg2bool(self.get_db_config(parameter),default)

    def set_db_config(self,parameter,wert,bedingung=''): 
        # wenn die bedingung nichtleer ist, dann wird das Setzen nur gemacht, 
        #   wenn der alte Wert genau gleich der bedingung ist.
        # Dies ermöglicht quasiatomare Änderungen von einem gegebenen/angenommenen Wert aus.
        
        wert=str(wert).strip()
        if not bedingung:
            self.conf_db_cursor.execute('INSERT OR REPLACE INTO configpairs (parameter,wert) VALUES (?,?)',(parameter,wert))
        else:
            self.conf_db_cursor.execute('''UPDATE configpairs 
                                        SET wert=?
                                        WHERE parameter=? AND wert=?''',(wert,parameter,bedingung))
        self.conf_db_conn.commit()
        
    def dump_db_config(self):
        self.conf_db_cursor.execute('SELECT * FROM configpairs ORDER BY parameter')
        return self.conf_db_cursor.fetchall()
    
    def get_gruppenbez(self,nr):
        gruppenname=['Q11','Q12','ClientTypeA','ClientTypeB','ClientTypeM','Lehrer','Soloscreen','Anzeige','Forward','RaspberryPi','hochkant']
        gruppenkuerzel=['Q11','Q12','A','B','M','Leh.','Solo','Anzg','Fwd','Rπ','hk.']  
    # die kompletten Namen sind in ti.py
        # für 2-4 sind die Namen die Parameter in der Config-Datenbank, welche dann die Namen liefert
        if nr in [2,3,4]:
            erg=self.get_db_config(gruppenname[nr],'CT'+gruppenname[nr][-1])
            return erg,erg[:4]
        if nr<len(gruppenname):
            return gruppenname[nr],gruppenkuerzel[nr]
        return "",""
        
    def remove_config_DEAD(self,parameter):
        self.conf_db_cursor.execute('DELETE FROM configpairs WHERE parameter=?',(parameter,))
        self.conf_db_conn.commit()

    def client_get_config(self,ip):
        self.conf_db_cursor.execute('SELECT name,timestamp,minphase,maxphase,gruppen FROM clients WHERE ip=?',(ip,))
        for row in self.conf_db_cursor:
            return row
        return 'Fehler',0,0,0,0

    def client_get_all(self,ip):
        """Liest aus der db die Clientkonfiguration aus."""
        self.conf_db_cursor.execute('SELECT name,timestamp,minphase,maxphase,gruppen,istanzg,cukopplung,forward FROM clients WHERE ip=?',(ip,))
        for row in self.conf_db_cursor:
            return row
        return 'Fehler',0,0,10,0,0,0,''

    def client_force_gruppen(self,clientgroups):
        self.clientgroups = clientgroups
        
    def client_get_gruppen(self,ip):
        if self.clientgroups>0: return self.clientgroups
        self.conf_db_cursor.execute('SELECT gruppen FROM clients WHERE ip=?',(ip,))
        for row in self.conf_db_cursor:
            return row[0]
        return 0

    def client_is_portrait(self,ip): # Display hochkant?
        self.conf_db_cursor.execute('SELECT gruppen FROM clients WHERE ip=?',(ip,))
        for row in self.conf_db_cursor:
            return (row[0]&1024)>0
        return False

    def client_get_forward(self,ip):
        self.conf_db_cursor.execute('SELECT forward FROM clients WHERE ip=?',(ip,))
        for row in self.conf_db_cursor:
            return row[0].strip()
        return ''

    def client_get_istanzg(self,ip):
        self.conf_db_cursor.execute('SELECT istanzg FROM clients WHERE ip=?',(ip,))
        for row in self.conf_db_cursor:
            return row[0]
        return 0

    def client_get_cukopplung(self,ip):
        self.conf_db_cursor.execute('SELECT cukopplung FROM clients WHERE ip=?',(ip,))
        for row in self.conf_db_cursor:
            return row[0]
        return 0
        
    def client_get_name(self,ip):
        self.conf_db_cursor.execute('SELECT name FROM clients WHERE ip=?',(ip,))
        for row in self.conf_db_cursor:
            return row[0]
        return ''

    def client_set_gruppen(self,ip,gruppen):
        self.conf_db_cursor.execute('UPDATE clients SET gruppen=? , istconf=1 WHERE ip=?',(gruppen,ip))
        self.conf_db_conn.commit()
        
    def client_set_forward(self,ip,fwd):
        self.conf_db_cursor.execute('UPDATE clients SET forward=? , istconf=1 WHERE ip=?',(fwd,ip))
        self.conf_db_conn.commit()
        
    def client_set_istanzg(self,ip,istanzg):
        self.conf_db_cursor.execute('UPDATE clients SET istanzg=? , istconf=1 WHERE ip=?',(istanzg,ip))
        self.conf_db_conn.commit()
        
    def client_set_cukopplung(self,ip,cukopplung):
        self.conf_db_cursor.execute('UPDATE clients SET cukopplung=? , istconf=1 WHERE ip=?',(cukopplung,ip))
        self.conf_db_conn.commit()
        
    def client_set_name(self,ip,name):
        self.conf_db_cursor.execute('UPDATE clients SET name=? , istconf=1 WHERE ip=?',(name,ip))
        self.conf_db_conn.commit()

    def client_get_list(self):
        self.conf_db_cursor.execute('SELECT ip,name,timestamp,maxphase,gruppen,istconf,istanzg,cukopplung,forward FROM clients ORDER BY istconf DESC, istanzg DESC, ip ASC')
        return self.conf_db_cursor.fetchall()
        
    def client_set_phaselimits(self,ip,mipha,mapha):
        self.conf_db_cursor.execute('UPDATE clients SET minphase=? , maxphase=?, istconf=1 WHERE ip=?',(mipha,mapha,ip))
        self.conf_db_conn.commit()

    def client_show_status(self,ti_ctx):
        import ti
        ti_ctx.print_html_head(timeout=20,url='ti_tool.py')
        t_schwelle=ti_lib.get_now()-self.get_db_config_int('SekundenBisAnzeigefehler')
        felddefinition='''
        <div style="float:left;margin:0.5em;border:1;border-style:solid;border-color:white;border-radius:5px;
        padding:5px;text-align:center;vertical-align:middle;
        width:10em;height:9em;'''
        self.conf_db_cursor.execute('SELECT ip,name,timestamp FROM clients WHERE istanzg>0 ORDER BY ip ASC')
        for row in self.conf_db_cursor:
            ip,name,timestamp=row
            lasttime=ti.get_datestring_from_epoche(timestamp)
            if timestamp<t_schwelle:
                farbe='orange'
                delta_t=int((ti_lib.get_now()-timestamp)/60)
                if delta_t<181:
                    kommentar='<br>vor '+str(delta_t)+' Minuten'
                else:
                    h=int(delta_t/60)
                    if h<6:
                        m=delta_t-60*h
                        kommentar='<br>vor '+str(h)+' Stunden, '+str(m)+' Minuten'
                    elif h<48:
                        kommentar='<br>vor über '+str(h)+' Stunden'
                    else:
                        kommentar='<br>vor über '+str(int(h/24))+' Tagen'
            else:
                farbe='lightgreen'
                kommentar=""
            ti.prt(felddefinition+'background-color:'+farbe+';"><br><b>'+name+'</b><br><small>('+ip+')</small><br>'+lasttime+kommentar+'</div>')
        ti.prt(felddefinition+'''background-color:grey;">
            <small>Status der Anzeigen!<br>Die Zeilen sind:<br>
            <b>Der Clientname</b>,<br><small>(die IP-Adresse)</small><br>Datum & Uhrzeit des letzten Zugriffs auf das Tabula-System.</small>
        </div>
        ''')
        ti_ctx.print_html_tail()
        
    def client_ping(self,ip,ti_response):
        # Hier wird nicht nur der aktuelle Zeitpunkt des Aufrufs vermerkt,
        # sondern ggf. auch erst ein neuer Eintrag für einen neuen client angelegt.
        ti_response.debug("request durch IP",ip)
        self.conf_db_cursor.execute('SELECT timestamp FROM clients WHERE ip=?',(ip,))
        _now_=time.time() #sec since epoche
        for result, in self.conf_db_cursor:
            if int(_now_)>int(result)+10: # frühestens nach 10 Sekunden aktualisieren wg. Denial of Service Gefahr
                try:
                    self.conf_db_cursor.execute('UPDATE clients SET timestamp=? WHERE ip=?',(int(_now_),ip))
                    self.conf_db_conn.commit()
                except Exception as e:
                    ti_lib.panic("Schreibzugriff auf Datenbank scheiterte",e)
            return
            
        # aha, es gibt den Client noch nicht
        try:
            # Namen ermitteln
            ti_response.debug("IP",ip)
            hostname,alias,adressen=socket.gethostbyaddr(ip)
            ti_response.debug("hostname",hostname)
            hostname=hostname.split(".")[0]
        except:
            hostname=ip
        try:
            self.conf_db_cursor.execute('INSERT INTO clients (ip,name,timestamp,maxphase) VALUES (?,?,?,?)',(ip,hostname,_now_,10))
            self.conf_db_conn.commit()
        except Exception as e:
            ti_response.debug('INSERT fehlgeschlagen',e,'')
        
            
    def client_remove(self,ip):
        self.conf_db_cursor.execute('DELETE FROM clients WHERE ip=?',(ip,))
        self.conf_db_conn.commit()

    def check_n_set_semaphore(self,name,onlycheck=False):
        now=int(time.time())
        timeout=self.get_db_config(name+"_sema")
        #print(timeout,">",now,"?")
        if timeout.isdigit():
            if now<int(timeout):
                return -1 # heisst: du musst noch warten
        if not onlycheck:
            self.set_db_config(name+"_sema",str(now+15*60))
        return 0 # heisst: du darfst
            
    def clear_semaphore(self,name):
        now=int(time.time())
        self.set_db_config(name+"_sema",str(now-1))
        
    def set_ti_lastupload(self):
        epoche=int(time.time())
        self.set_db_config('uploadtimestamp',epoche)
        self.set_db_config('uploadtimestamp_klartext',time.strftime('%Y-%m-%d_%T', time.localtime(epoche)))
