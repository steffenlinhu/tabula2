#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""ti2.py ist der zentrale Aufruf von/für tabula.info in der Version 2
   Neu in Version 2: Aufruf via wsgi, systematischere Content-Verteilung
"""
# Batteries (included)
import os 
import sys
#import urlparse

# Importe von werkzeug (per apt installiert)
from werkzeug.wrappers   import Request, Response
from werkzeug.routing    import Map, Rule
from werkzeug.exceptions import HTTPException, NotFound
from werkzeug.middleware.shared_data import SharedDataMiddleware
from werkzeug.utils      import redirect

# Importe ti2 (eigene Module)
import html_barebone2
import ajax2

# Importe von ti-classic - die Schnittstelle
import ti_lib
import config_db
import ti_init
import ti_context
import messages         # liefert die Nachrichten
import persons          # liefert den Personenruf


def hacklog(*args):
    sys.stderr.write("!! {}\n".format(": ".join([str(i) for i in args])))
    sys.stderr.flush()
    
class TiApp(object):

    def __init__(self, config):
        # links URL-Pfad bezogen auf das aufgerufene Skript
        # Pfade kommen doppelt vor, damit per mod_wsgi@apache und per run_simple (s.u.) lauffähig
        self.url_map = Map([
            Rule('/ti',             endpoint='forward'),
            Rule(   '/',            endpoint='forward'),
            Rule('/ti/frontend',    endpoint='frontend'), # für run_simple
            Rule(   '/frontend',    endpoint='frontend'), # für mod_wsgi (das als /ti läuft)
            Rule('/ti/ajax',        endpoint='ajax'),
            Rule(   '/ajax',        endpoint='ajax'),
            Rule('/ti/manage',      endpoint='manage'),
            Rule(   '/manage',      endpoint='manage'),
        ])
        hacklog("ti.app initialisiert")
        

    def dispatch_request(self, request):
        hacklog('URL:',request.environ['REQUEST_URI'])
        adapter = self.url_map.bind_to_environ(request.environ)
        try:
            endpoint, values = adapter.match()
            hacklog("erkannter Aufruf",endpoint)
            return getattr(self, 'do_' + endpoint)(request, **values)
        except HTTPException as e:
            return e

    def wsgi_app(self, environ, start_response):
        request = Request(environ)
        
        ti_sys=ti_init.TI_System() # Basis
        ti_cnf=config_db.TI_Config() # Config-Datenbank
        ti_res=ti_init.TI_Response(ti_sys.get_epoche()) # für die Ausgabe
        ti_req=ti_init.TI_Request_WSGI(request,ti_cnf,ti_res)  # der Request
        #hacklog("Client",ti_req.client.ip_address)
        #None statt ti_ses=login.TI_Session(ti_req,ti_cnf,ti_sys.get_epoche())

        self.ti_ctx=ti_context.TIConTeXt(ti_sys,ti_cnf,ti_req,ti_res,None) # sollte nun jedem Tool zur Verfügung gestellt werden können

        hacklog("ti.app request angekommen und response vorbereitet")
        ## Hier wird die eigentlich gewünschte Funktion aufgerufen und das Ergebnis zurückgegeben
        response = self.dispatch_request(request)
        
        return response(environ, start_response)

    def __call__(self, environ, start_response):
        return self.wsgi_app(environ, start_response)

    def do_forward(self,request):
        return Response(html_barebone2.root_forward,mimetype='text/html')
        
    def do_frontend(self,request):
        return Response(html_barebone2.frontend,mimetype='text/html')
        
    def do_manage(self,request):
        return Response(html_barebone2.manage,mimetype='text/html')
        
    def do_ajax(self,request):
        return Response(ajax2.do_ajax(self.ti_ctx),mimetype='application/json')
        
    #def __call__(self, environ, start_response):
    #    return self.wsgi_app(environ, start_response)


def create_app(selfcontained=False):
    #eigentliche App anlegen
    app = TiApp({
        'redis_host':       123,
        'redis_port':       8000
    })
    #App ergänzen, damit statische Inhalte unterhalb von www gezeigt werden
    if selfcontained:
        hacklog(_('App liefert statischen Content selbst per WSGI aus.'))
        app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
            '/static':  os.path.join(os.path.dirname(__file__), '..', 'www','static'),
            '/dyn':     os.path.join(os.path.dirname(__file__), '..', 'www','dyn'),
            '/local':   os.path.join(os.path.dirname(__file__), '..', 'www','local'),
        })
    return app
    
if __name__ == '__main__':
    from werkzeug.serving import run_simple
    app = create_app(True)
    run_simple('0.0.0.0', 5000, app, use_debugger=True, use_reloader=True, threaded=True)
