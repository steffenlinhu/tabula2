#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
________________________________________________________________________
 config_clients.py                                                        
 This file is free software under         
     the terms of the GPL without any warranty - see the file COPYING 
 Diese Datei ist  Freie Software gemäß
             der GPL ohne jegliche Garantie - siehe die Datei COPYING 
________________________________________________________________________

 config_clients.py vereint den Dialog und die Tätigkeiten
            zur Konfiguration und Überwachung der Clients
________________________________________________________________________
'''

# Batteries
# (nix)
# TI
import ti_lib
import ti
import config
import konst


def config_dialog(ti_ctx):
    tosay=[]
    checkedsessionid=ti_ctx.get_checkedsessionid()
    clientverwaltungs_div_definition='<div style="float:left;margin:0.5em;border:1;background-color:#CC8;border-style:solid;border-color:blue;border-radius:5px;padding:5px;font-size:70%;text-align:right;">'
    ## Clientverwaltung width:90%;
    alleclients=ti_ctx.cnf.client_get_list() # diese Liste enthält Tupel. Sortiert ist sie nach istconf, dann nach istanzg
    #ti_ctx.res.debug('AlleUser:',str(alleuser))
    statemachine=0 # null: noch keine Überschrift geschrieben, 1: hat "konfigurierte Anzeigen" geschrieben, 2: hat "konfigurierte Clients geschrieben, 3: hat "unkonf. C." geschrieben
    for tupel in alleclients:
        ip,name,timestamp,maxphase,gruppen,istconf,istanzg,cukopplung,forward=tupel
        #ti_ctx.res.debug('nächster Client (n,t,m,g,i)',str(tupel))
        if statemachine==0 and istconf and istanzg:
            tosay.append(clientverwaltungs_div_definition)
            tosay.append('<h3 style="text-align:left;margin-bottom:0.5em;">Kontext: '+'verwaltete Anzeigerechner'+'\n')
            tosay.append(config.popuphilfe('Anzeigerechner','per IP-Adresse identifizierte Clients, welche als öffentlich Anzeigen dienen. Deren Funktion kann überwacht werden. Rufe ti_tool.py auf!')+'</h3>')
            statemachine=1
        elif statemachine<=1 and istconf and not istanzg:
            if statemachine>0: # der Vorgänger wurde genutzt
                tosay.append('</div>\n')
            tosay.append(clientverwaltungs_div_definition)
            tosay.append('<h3 style="text-align:left;margin-bottom:0.5em;">Kontext: '+'verwaltete Clients'+'</h3>\n')
            statemachine=2
        elif statemachine<=2 and not istconf:
            if statemachine>0:
                tosay.append('</div>\n') # ein Vorgänger wurde genutzt
            tosay.append(clientverwaltungs_div_definition)
            tosay.append('<h3 style="text-align:left;margin-bottom:0.5em;">Kontext: '+'unkonfigurierte Clients'+'</h3>\n')
            statemachine=3 # final
        
        ## Anfang einer Clientbeschreibung
        tosay.append('<div style="float:left;width:10em;padding:0.3em;background-color:#FFD">')     
        tosay.append(ip+'&nbsp;&nbsp;')
        tosay.append('</div>')
        # Beginn des umfassenden DIVs um die Clientdetails
        tosay.append('<div style="float:left;background-color:#FFD;margin-bottom:0.5em;padding:0.3em;">')
        tosay.append('<div style="float:left;">')
        tosay.append('<form action="config_clients.py" method="post" >')
        tosay.append('<input name="clientip" value="'+ip+'" type="hidden" />')
        tosay.append('<input name="newname" value="'+name+'" type="text" size="8" maxlength="16" />')
        tosay.append('<input name="easid" value="'+checkedsessionid+'" type="hidden"><input value="Umbenennen" type="submit">')
        tosay.append('</form></div>')   
        tosay.append('<div style="float:left;">')       
        tosay.append(config.popuphilfe('Umbenennen','Jeder Client erhält beim ersten Zugriff seinen Hostnamen, wenn er im DNS gefunden wird. Dieser kann geändert werden. Er wird z.B. beim Anzeigenstatus angezeigt und bei über den Client legitimierten Meldungen als Autor angegeben'))
        tosay.append('</div>')
        
        tosay.append('<div style="float:left;">')       
        tosay.append('<form action="config_clients.py" method="post" >&nbsp;')
        tosay.append('<input name="ip2remove" value="'+ip+'" type="hidden" />')
        tosay.append('<input name="easid" value="'+checkedsessionid+'" type="hidden" />')
        tosay.append('<input value="Löschen" type="submit" style="background-color:red;color:white;font-weight:bold;" />')
        tosay.append('</form></div>')
        tosay.append('<div style="float:left;">')       
        tosay.append(config.popuphilfe('Client löschen','Der Client wird aus der Datenbank entfernt, bis er selber wieder auf das System zugreift.'))
        tosay.append('</div>')
        
        tosay.append('<div style="float:left;">')       
        tosay.append('<form action="config_clients.py" method="post" >&nbsp;')
        tosay.append('<input name="easid" value="'+checkedsessionid+'" type="hidden" />')
        tosay.append('<input name="cukopplung_ip" value="'+ip+'" type="hidden" />')
        if cukopplung:
            tosay.append('<input value="Kopplung besteht" type="submit" style="background-color:green;font-weight:bold;" />')
        else:
            tosay.append('<input value="keine Kopplung" type="submit" />')
        tosay.append('</form></div>')
        tosay.append('<div style="float:left;">')       
        tosay.append(config.popuphilfe('Kopplung','Ein Anwender, der diesen Client zum Managen benutzt erhält die Rechte des <i>gleichnamigen</i> Users - wenn einer existiert...<br><br>Funktioniert aus Sicherheitsgründen nur im ManagedNetwork!'))
        tosay.append('</div>')

        

        
        tosay.append('<div style="float:left;">')   
        warheute=ti.get_dayofepoche(timestamp)==ti.get_todays_dayofepoche()
        langher=ti.get_dayofepoche(timestamp)<ti.get_todays_dayofepoche()-7
        if warheute:
            tosay.append('<b style="color:green">')
        elif langher:
            tosay.append('<span style="color:grey">')
        tosay.append(' &nbsp; <small><small>Letzter Zugriff: '+ti.get_datestring_from_epoche(timestamp)+'</small></small>')
        if warheute:
            tosay.append('</b>')
        elif langher:
            tosay.append('</span>')
        tosay.append(' </div>')
        #zweite Zeile beginnen
        tosay.append('<div style="clear:left;font-size:1px;">&nbsp;</div>')
        if istanzg:
            gruppen|=128
        tosay.append(config.htmloptionenzeile( \
                gruppen, ti_ctx.cnf.get_gruppenbez,
                'config_clients.py', 'gruppentoggle', 'clientip', ip,
                checkedsessionid,
                beschreibung=('Grp.','Eigensch.',64)))
        
        gruppennamenhilfe='Clientgruppen:<br><br>'
        for i in range(20):
            gn,gk=ti_ctx.cnf.get_gruppenbez(i) # GruppenName,GruppenKürzel
            if gn:
                gruppennummer=1 << i
                if gruppennummer==64:
                    gruppennamenhilfe+='<br>Eigenschaften:<br>'
                if gruppennummer<64:
                    gruppennamenhilfe+=gk+': '+('Zeige Meldungen für ' if gruppen & gruppennummer else 'Verberge Meldungen für ')+gn+'<br>'
                else:
                    gruppennamenhilfe+=gk+': '+('Client ist vom Typ ' if gruppen & gruppennummer else 'Client ist kein  ')+gn+'<br>'
        tosay.append('<div style="float:left;">')       
        tosay.append(config.popuphilfe('Aktueller Stand:',gruppennamenhilfe))
        tosay.append('</div>')
        
        #dritte Zeile für Anzeigeneinstellungen
        if istanzg:
            mipha,mapha=ti_ctx.get_client_phaselimits(ip)
            tosay.append('<div style="clear:left;font-size:1px;">&nbsp;</div>')
            tosay.append('<div style="float:left;">')
            tosay.append('<form action="config_clients.py" method="post" >Anzeigebereich:')
            tosay.append('<input name="clientip" value="'+ip+'" type="hidden" />')
            tosay.append('<input name="minphase" value="'+str(mipha)+'" type="text" size="1" maxlength="2" /> bis ')
            tosay.append('<input name="maxphase" value="'+str(mapha)+'" type="text" size="1" maxlength="2" />')
            tosay.append('<input name="easid" value="'+checkedsessionid+'" type="hidden"><input value="OK" type="submit">')
            tosay.append('</form></div>')   
            tosay.append('<div style="float:left;">')       
            tosay.append(config.popuphilfe('Anzeigebereich','''Der Bereich startet bei 0 für den Aushang.
            Höhere Nummern stehen für die Pläne. 
            Hiermit kann für diesen Client eingeschränkt werden, was er anzeigen soll.
            Die angegebenen Grenzen sind inklusiv!
            Passt ein folgender Plan noch mit auf die Seite des Bereichsendes, so wird er auch angezeigt!'''))
            tosay.append('</div>')
            
        
        if gruppen & 256: #vierte Zeile einfügen für Eingabe des Forward-Ziels
            tosay.append('<div style="clear:left;text-align:left;">&nbsp;</div>')
            tosay.append('<div style="float:left;">')
            tosay.append('<form action="config_clients.py" method="post" >')
            tosay.append('<input name="clientip" value="'+ip+'" type="hidden" />')
            tosay.append('<input name="forward" value="'+forward+'" type="text" size="18" maxlength="255" />')
            tosay.append('<input name="easid" value="'+checkedsessionid+'" type="hidden"><input value="Umleiten des Clients" type="submit">')
            tosay.append('</form>') 
            tosay.append('</div>') # Ende dritte Zeile
        
        # Ende des umfassenden DIVs um die Clientdetails
        tosay.append('</div>')
        tosay.append('<div style="clear:left;font-size:1px;">&nbsp;</div>')

    if statemachine==0:
        tosay.append(clientverwaltungs_div_definition)
        tosay.append('<h3 style="text-align:left;">Es sind noch keine Clients erkannt worden.</h3>\n')
    tosay.append('</div>') # schließt Clientverwaltung
    return '\n'.join(tosay)
    

def status_info(ti_ctx):
    tosay=[]
    items2show=[ ('uploadtimestamp_klartext','Letzter Upload (Info o. V-plan)'),
                #('',''),
                #('',''),
                #('',''),
    ]
    tosay.append('''<div style="float:left;
                                margin:0.5em;border:1;background-color:lightblue;
                                border-style:solid;border-color:blue;border-radius:5px;
                                padding:5px;text-align:right;">
                        <h3 style="text-align:left;">Status des Systems</h3>
                        <table>''')
    for parameter,beschreibung in items2show:
        wert=ti_ctx.cnf.get_db_config(parameter,'(nicht gesetzt)') # muss ja nicht klappen
        tosay.append(statuszeile(beschreibung,wert))
        
    #handselektierte Stati:
    import login
    tosay.append(statuszeile('Aktueller User',ti_ctx.ses.get_session_user()))
       
    tosay.append('</table></div>') # schließt Statusanzeige
    return '\n'.join(tosay)
    
def statuszeile(beschreibung,wert):
    return '<tr><td style="text-align:right">' \
            +beschreibung \
            +':&nbsp;</td><td style="text-align:left"> ' \
            +str(wert) \
            +'</td></tr>'
    
def config_todo(ti_ctx):
    ea_erfolg=""
    eapp=""
    easid=ti_ctx.req.get_cgi('easid')
    ti_ctx.res.debug('easid',easid)
    if len(easid)>5:
        import login
        if easid==ti_ctx.get_checkedsessionid():
            ## Ab hier werden die Eingaben verarbeitet
            clip=ti_ctx.req.get_cgi('clientip')
            #if len(clip.split('.'))==4:
            newname=ti_ctx.req.get_cgi('newname')
            if len(newname)>0:
                ti_ctx.cnf.client_set_name(clip,newname)
                return '+Gesetzt wurde als neuer Name für IP-Adresse '+clip+': '+newname

            mipha=ti_ctx.req.get_cgi_int('minphase',0)
            mapha=ti_ctx.req.get_cgi_int('maxphase',-1)
            if mapha>=0: # negative minphase sind nun erlaubt für invertieren des Bereichs
                if mipha>mapha: mipha,mapha=mapha,mipha
                ti_ctx.cnf.client_set_phaselimits(clip,mipha,mapha)
                return '+Anzeigebereich des Clients '+clip+' geändert von '+str(mipha)+' bis '+str(mapha)
                
            gruppentoggle=ti_ctx.req.get_cgi_int('gruppentoggle')
            if (gruppentoggle>0 and gruppentoggle<65) or gruppentoggle in [256,512,1024,2048]: # bei den großen immer nur ein Bit toggeln
                gruppen=ti_ctx.cnf.client_get_gruppen(clip)
                gruppen=gruppen ^ gruppentoggle # xor
                ti_ctx.cnf.client_set_gruppen(clip,gruppen)
                return '+Gruppenzugehörigkeit des Clients '+clip+' geändert'
                
            if gruppentoggle==128: # Gibt an, ob der Client ein zu überwachender AnzeigePC ist
                istanzg=not ti_ctx.cnf.client_get_istanzg(clip)
                ti_ctx.cnf.client_set_istanzg( clip, istanzg)
                return '+Client '+clip+' ist nun '+("k" if not istanzg else "")+'eine zu überwachende Anzeige'
                
            cukopplung_ip=ti_ctx.req.get_cgi('cukopplung_ip')
            if len(cukopplung_ip)>4: 
                cukopplung=not ti_ctx.cnf.client_get_cukopplung(cukopplung_ip)
                ti_ctx.cnf.client_set_cukopplung(cukopplung_ip,cukopplung)
                name=ti_ctx.cnf.client_get_name(cukopplung_ip)
                return '+Client '+cukopplung_ip+' ist '+("nicht mehr " if not cukopplung else "")+'mit '+name+' gekoppelt'
                
            ip2remove=ti_ctx.req.get_cgi('ip2remove')
            if len(ip2remove)>0:
                ti_ctx.cnf.client_remove(ip2remove)
                return '+Der Client mit der IP-Adresse '+ip2remove+' wurde aus der Clientliste entfernt'
                
            fwd=ti_ctx.req.get_cgi('forward','invalid')
            if fwd!='invalid':
                ti_ctx.cnf.client_set_forward(clip,fwd)
                return '+Der Client '+clip+' wird zukünftig auf "'+fwd+'" umgeleitet'
            return '-Kein interpretierbarer Parameter übergeben ('+easid+')'

            ## Abbruch
        else:
            return '-Falsche Session-ID! Nicht aus dem Browsercache aufrufen...'
    
    return '' # wurde garnicht mit Formular aufgerufen
    

HTML_TEMPLATE = '''
<div class="firstcontent" style="background:white;margin-bottom:0.5em;">
    <h1>Einstellungen für Clientrechner</h1>
</div>
<div class="firstcontent" style="background:white;">
    {dialog}
    <div style="clear:left;"> </div>
    <p>Anleitung:</p>
    <p><small>Der Name wird im ManagementNetwork als Verfassername für Meldungen verwendet, 
        sofern keine Anmeldung stattfand. Außerdem kann ein Client mit einem Useraccount ge<b>koppelt</b> werden, 
        wenn sie a) denselben Namen haben und b) der Client im ManagementNetwork liegt. 
        Dann erhält dieser Client sowohl die Rechte des ManagementNetworks als auch die Rechte des gleichnamigen Users. </small></p>
    <p><small>Die Clienttypen geben wieder, ob zielspezifische Zusatzinfos eingeblendet werden sollen,
        z.B. für Oberstufe, Lehrerzimmer usw.. 
        Einige dieser Gruppen können mit eigenen Namen versehen werden (-> Einstellungen) und 
        dann konsistent in dem Meldungseditor und auf den Anzeigen verwendet werden.</small></p>
    <p><small>Ein Client kann zu vielen Typen gehören, was aber der Übersicht in der Anzeige nicht dienlich ist...</small></p>
    <p><small>Ist ein Client eine Anzeige (<b>Anzg</b>), so kann sein regelmäßiger Datenabruf mit ti_tool.py überprüft werden.</small></p>
    <p>Wichtig: Eine Clientkonfiguration ist nur sinnvoll, wenn die betroffenen Clients <b>feste IP-Adressen</b> haben!</p>
    <p>Alternative: Starten Sie auf dem Clientbrowser die Seite http://tabulaserver/cgi-bin/frames.py?setclientgroups=32;sc_code={sc_code}</p>
    <p>Damit wird ein Lehrerclient simuliert. Der Code ist zufällig generiert und spezifisch für diese Installation.Mit setclientgroups=1 bzw. 2 bzw. 3 wird Q11 bzw. Q12 bzw. beide angesteuert usw.</p>
    <br>
    {stati}
</div>'''




def do_config(ti_ctx):
    sc_code=ti_ctx.cnf.get_db_config_int('sc_code')
    if sc_code<100001:
        import random
        ti_ctx.cnf.set_db_config('sc_code',str(int(100000+random.random()*900000)))
        sc_code=ti_ctx.cnf.get_db_config_int('sc_code')
    erfolg=config_todo(ti_ctx)
    ti_ctx.res.debug("Erfolg",erfolg)
    if erfolg:
        ti_ctx.set_erfolgsmeldung(erfolg)
    ti_ctx.print_html_head(ismanagement=True)
    ti.prt(HTML_TEMPLATE.format(css=ti_ctx.standardcss(),dialog=config_dialog(ti_ctx),sc_code=sc_code,stati=status_info(ti_ctx)))
    ti_ctx.print_html_tail()


if __name__ == '__main__':
    ti_ctx=ti.master_init()
    try:
        ti_ctx.check_management_rights(konst.RECHTEAdministration)
        import login
        do_config(ti_ctx)
    except Exception as e:
        ti_lib.panic('config_clients abgestuerzt:',ti_lib.beschreibe_exception(e))
