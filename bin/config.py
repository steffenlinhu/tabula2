#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
________________________________________________________________________
 config.py                                                        
 This file is free software under         
     the terms of the GPL without any warranty - see the file COPYING 
 Diese Datei ist  Freie Software gemäß
             der GPL ohne jegliche Garantie - siehe die Datei COPYING 
________________________________________________________________________

 config.py vereint den Dialog und die Tätigkeiten zur Konfiguration
________________________________________________________________________
'''

# Batteries
# (nix)
# TI
import ti, ti_lib
import login
import konst
    

def htmloptionenzeile(bitmuster, bez_fkt, url, inputname, hiddenname, hiddenvalue, sid,autobreak=False,beschreibung=('','',0)):
    '''gibt 
        Buttons aus, die nach dem 
        Bitmuster eingefärbt sind und mit den 
        kuerzeln, welche die bez_fkt als zweiten Wert zurückliefert, beschriftet sind.
    Ein Klick ruft die 
        url auf, wobei drei Werte übertragen werden: versteckt der
        hiddenname mit dem hiddenvalue, sowie
        easid mit dem sid sowie als
        inputname der angeklickte Wert (eine Zweierpotenz)'''
    
    liste=['<!-- eine htmloptionenzeile aus config.py -->']
    static=[]
    static.append('<div style="float:left;width:{weite}em;text-align:left;">')        
    static.append('\t<form action="'+url+'" method="post" >')
    static.append('\t<input name="'+hiddenname+'" value="'+hiddenvalue+'" type="hidden" />')
    static.append('\t<input name="easid" value="'+sid+'" type="hidden" />') 
    staticcontent="\n".join(static)
    
    for i in range(20):
        gn,gk=bez_fkt(i) # GruppenName,GruppenKürzel
        if gk:
            bitwert= 1 << i
            wert=bitmuster & bitwert
            weite=3.5
            if beschreibung[2] and bitwert==1:
                beschr=beschreibung[0]+':'
                weite+=len(beschreibung[0])*0.6
            elif beschreibung[2] == bitwert:
                beschr=beschreibung[1]+':'
                weite+=len(beschreibung[1])*0.6
            else:
                beschr=''
            liste.append(staticcontent.format(weite=weite))
            liste.append(beschr)
            liste.append('\t<input name="'+inputname+'" value="'+str(bitwert)+'" type="hidden" />')
            liste.append('\t<input style="background:'+ \
                            ('green' if wert else 'orange')+ \
                            (';' if not wert else ';font-weight:bold;')+ \
                            '" value="'+gk+'" type="submit" />')
            liste.append('</form></div>')
    return '\n'.join(liste)

def config_dialog(ti_ctx):
    tosay=[]
    checkedsessionid=ti_ctx.get_checkedsessionid()
    kats=konst.get_configkategorien() # liefert ein Array aus Tupeln (kategoriename,config_struct)
    kategoriekastenbreite='62em'
    kategoriekastendefinition=  '''<div style="float:left;margin:0.5em;border:1;
                                        background-color:#FFA;min-width:30em;
                                        border-style:solid;border-color:green;border-radius:5px;
                                        padding:5px;font-size:70%;text-align:right;">'''
    for kname,cstruct in kats:
        tosay.append(kategoriekastendefinition)
        tosay.append('<h3 style="text-align:left;">Kontext: '+kname+'</h3>\n')
        cstruct.sort()
        for parameter,chelp,cstandard in cstruct:
            if parameter.startswith('!INFO'):
                tosay.append('<div style="float:left;width:35em;text-align:justify;">'+chelp+'</div>')
                tosay.append('<div style="clear:left;font-size:1px;">&nbsp;</div>')
                continue
            chelp+='<br>Standardwert: '+ ( cstandard if cstandard else '&lt;keiner&gt;')
            if parameter[0]=='+':
                parameterlaenge=5
                eingabelaenge=35
                maximallaenge=250
                parameter=parameter[1:]
            else:
                parameterlaenge=19
                eingabelaenge=10
                maximallaenge=50
            
            if ti_ctx.cnf.get_db_config(parameter,'keinWert!')=='keinWert!':
                ti_ctx.cnf.set_db_config(parameter,cstandard) # hiermit wird jeder Parameter, der erklärt wird auch angelegt!
            if parameter.lower()[:5]=='flag_':
                anzeigeparameter=parameter[5:]
                if ti_ctx.cnf.get_db_config_bool(parameter,False):
                    ti_ctx.cnf.set_db_config(parameter,'True') 
                else:
                    ti_ctx.cnf.set_db_config(parameter,'False') 
            else:
                anzeigeparameter=parameter
            if anzeigeparameter.startswith('s_any_'):
                anzeigeparameter=anzeigeparameter[6:]
            wert=ti_ctx.cnf.get_db_config(parameter) # muss jetzt klappen
            
            tosay.append('<div style="float:left;width:35em;text-align:right;">')
            
            tosay.append('<form action="config.py" method="post" >')
            tosay.append(anzeigeparameter+'<input name="paerchen_parameter" value="'+parameter+'" type="hidden">')
            if parameter.lower()[:5]=='flag_':
                checked=" checked=checked " if ti.strg2bool(wert) else ""
                tosay.append('<input name="paerchen_wert"  type="checkbox" '+checked+' />')
            else:
                tosay.append('<input name="paerchen_wert" value="'+wert+'" type="text" size="'+str(eingabelaenge)+'" maxlength="'+str(maximallaenge)+'" />')
            tosay.append('<input name="easid" value="'+checkedsessionid+'" type="hidden"><input value="OK" type="submit">')
            tosay.append('</form>')
            tosay.append('</div>')
            
                        
            tosay.append('<div style="float:left;">')       
            tosay.append('<form action="config.py" method="post" >')
            tosay.append('<input name="paerchen_parameter" value="'+parameter+'" type="hidden" />')
            tosay.append('<input name="paerchen_wert" value="'+cstandard+'" type="hidden" />')
            tosay.append('<input name="easid" value="'+checkedsessionid+'" type="hidden"><input style="font-size:60%;" value="Standard" type="submit">')
            tosay.append(popuphilfe(parameter,chelp))
            tosay.append('</form>')
            tosay.append('</div>')
            
            tosay.append('<div style="clear:left;font-size:1px;">&nbsp;</div>')
            
        tosay.append('</div>')
        
    ## Benutzerverwaltung
    #tosay.append('<div style="float:left;margin:0.5em;border:1;background-color:yellow;width:'+kategoriekastenbreite+';border-style:solid;border-color:green;border-radius:5px;padding:5px;font-size:70%;text-align:right;">')
    tosay.append(kategoriekastendefinition)
    tosay.append('<h3 style="text-align:left;">Kontext: '+'Benutzerverwaltung'+'</h3>\n')
    import login
    alleuser=ti_ctx.ses.get_userlist()
    ti_ctx.res.debug('AlleUser:',str(alleuser))
    for tupel in alleuser:
        name,rechte=tupel
        ti_ctx.res.debug('name und rechte',name+'#'+str(rechte))
        rechtenamenhilfe='Aktueller Stand:<br><br>'
        for i in range(10):
            rn,rk=login.get_rechte_bez(i)
            if rn:
                rechtenamenhilfe+=rk+': '+rn+' '+('erlaubt' if rechte&login.get_rechte_nr(i) else 'verboten')+'<br>'
    
        tosay.append('<div style="float:left;width:10em;text-align:right;">')       
        tosay.append(name+'&nbsp;&nbsp;')
        tosay.append('</div>')
        
        tosay.append(htmloptionenzeile(rechte, login.get_rechte_bez, 'config.py', 'rechtetoggle', 'username', name, checkedsessionid))
        tosay.append('<div style="float:left;">')       
        tosay.append(popuphilfe('Benutzerrechte',rechtenamenhilfe))
        tosay.append('</div>')
        
        if name!='vianetwork':
            ## Passwort zurücksetzen
            tosay.append('<div style="float:left;">')       
            tosay.append('<form action="config.py" method="post" >&nbsp;&nbsp;')
            tosay.append('<input name="user2reset" value="'+name+'" type="hidden" />')
            tosay.append('<input name="easid" value="'+checkedsessionid+'" type="hidden" />')
            tosay.append('<input value="PWRst" type="submit" />')
            tosay.append('</form></div>')
            tosay.append('<div style="float:left;">')       
            tosay.append(popuphilfe('Passwort-Reset','Das Passwort wird auf den Namen "tabula.info" zurückgesetzt'))
            tosay.append('</div>')
            ## User löschen
            tosay.append('<div style="float:left;">')       
            tosay.append('<form action="config.py" method="post" >&nbsp;&nbsp;')
            tosay.append('<input name="user2delete" value="'+name+'" type="hidden" />')
            tosay.append('<input name="easid" value="'+checkedsessionid+'" type="hidden" />')
            tosay.append('<input value="Lösch" type="submit" style="background-color:red;color:white;font-weight:bold;" />')
            tosay.append('</form></div>')
            tosay.append('<div style="float:left;">')       
            tosay.append(popuphilfe('User löschen','Ein User, der nicht mehr benötigt wird, kann hier ohne Rückfrage gelöscht werden!'))
            tosay.append('</div>')
        else:
            tosay.append('<div style="float:left;"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </div>')       
            tosay.append('<div style="float:left;">')       
            tosay.append(popuphilfe('kein Passwort, kein Löschen','Die Berechtigung wird in der ti-Basiskonfiguration über das "ManagementNetwork" geregelt. Hier wird nur festgelegt, welche Rechte so ein Benutzer erhält. Diese Rechte kumulieren mit seinen durch Anmeldung nachgewiesenen Rechten.'))
            tosay.append('</div>')
            
        tosay.append('<div style="clear:left;font-size:1px;">&nbsp;</div>')

    tosay.append('<div style="float:left;">')
    tosay.append('<form action="config.py" method="post" >')
    tosay.append('Neuer User: <input name="newuser" value="neuername" type="text" size="10" maxlength="16" />')
    tosay.append('<input name="easid" value="'+checkedsessionid+'" type="hidden"><input value="Anlegen" type="submit">')
    tosay.append('</form>')
    tosay.append('</div><div style="clear:left;font-size:1px;">&nbsp;</div>')
        
    tosay.append('</div>') # schließt Benutzerverwaltung
    tosay.append('<div style="clear:left;font-size:1px;">&nbsp;</div>')
    tosay.append('''<div>
                    <p>Anleitung:<br><small>
                    Jede einzelne Eingabemöglichkeit hat ihre eigene Hilfe.<br>
                    Es wird jeweils ein typischer Standardwert vorgeschlagen<br>
                    Jede einzelne Änderung muss mit Klick auf "OK" o.ä. abgeschickt werden!</small></p></div>''')
    tosay.append('''<div><p>Bei Problemen können Sie eine <a href="config.py?debugfile=true" target="_blank">Datei</a> mit Informationen zu Ihrem System erstellen lassen und in einer Mail an den Programmautor im Anhang übertragen.</p></div>''')
    return '\n'.join(tosay)
    
def popuphilfe(titel,hilfetext):
    return '<popuphelp><span><b>'+titel+'</b><br>'+hilfetext+'</span>&nbsp;<b>?</b>&nbsp;</popuphelp><br>'

    
def config_todo(ti_ctx):
    ea_erfolg=""
    eapp=""
    easid=ti_ctx.req.get_cgi('easid')
    ti_ctx.res.debug('easid',easid)
    if len(easid)>5:
        import login
        if easid==ti_ctx.get_checkedsessionid():
            ## Ab hier werden die Eingaben verarbeitet
            eapp=ti_ctx.req.get_cgi('paerchen_parameter')
            if len(eapp)>0:
                eapw=ti_ctx.req.get_cgi('paerchen_wert')
                if eapp.startswith('s_any_') or eapp.startswith('flag_s_any_'):
                    ti_ctx.cnf.set_db_config('s_any_force_convert',1)
                    ti_ctx.cnf.set_db_config('background_timeout',0)
                if eapp[:5].lower()=='flag_':
                        if eapw:
                            ti_ctx.cnf.set_db_config(eapp,"1")
                            return '+Eingeschaltet wurde: '+eapp
                        else:
                            ti_ctx.cnf.set_db_config(eapp,"0")
                            return '+Ausgeschaltet wurde: '+eapp
                ti_ctx.cnf.set_db_config(eapp,eapw)
                return '+Gesetzt wurde: '+eapp+'="'+eapw+'"'
            username=ti_ctx.req.get_cgi('username')
            if len(username)>0:
                rechtetoggle=ti_ctx.req.get_cgi_int('rechtetoggle')
                if rechtetoggle>0 and rechtetoggle<65:
                    rechte=ti_ctx.ses.get_userrechte(username)
                    rechte=rechte ^ rechtetoggle # xor
                    ti_ctx.ses.set_userrechte(username,rechte)
                    return '+Rechte geändert'
                else:
                    return '-keine Rechte geändert'
            user2reset=ti_ctx.req.get_cgi('user2reset')
            if len(user2reset)>0:
                ti_ctx.ses.reset_userpasswd(user2reset)
                return '+Passwort von '+user2reset+' wurde zurückgesetzt'
            user2delete=ti_ctx.req.get_cgi('user2delete')
            if len(user2delete)>0:
                ti_ctx.ses.remove_dbuser(user2delete)
                return '+Der User '+user2delete+' wurde gelöscht'
            newuser=ti_ctx.req.get_cgi('newuser')
            if len(newuser)>0:
                ti_ctx.ses.adduser(newuser)
                return '+'+newuser+' wurde mit Standardpasswort angelegt'
            # Sonst...
            return '-Kein Parameter übergeben ('+easid+')'

            ## Abbruch
        else:
            return '-Falsche Session-ID! Nicht aus dem Browsercache aufrufen...'
        
    return '' # wurde garnicht mit Formular aufgerufen
    
def make_debugfile(ti_ctx):
    import codecs
    import subprocess
    datei=['''<!DOCTYPE html>\n<html>
    <head>
        <meta content="text/html; charset=utf-8" http-equiv="content-type">
    </head>
    <body>
    <h1>Debuginformationen</h1>''']
    datapath=ti.get_ti_datapath()
    basepath=ti.get_ti_basepath()
    for filename,color in [('log/background.log','lightgreen'),('log/background.log.old','lightyellow')]:
        chunk=['<h2 style="background-color:{col};">Datei: {path}</h2>\n<pre style="background-color:{col};">'.format(col=color,path=datapath+filename)]
        try:
            with codecs.open(datapath+filename, mode='r', encoding='utf-8') as fin:
                for line in fin:
                    chunk.append(line)
                fin.close()
            datei.append('\n'.join(chunk)+'\n</pre>')
        except Exception as e:
            import ti_lib
            datei.append('Datei konnte nicht gelesen werden<br>'+'<br>\n'.join(chunk)+'\n</pre>\n'+ti_lib.beschreibe_exception(e))
    for command in ['date','tree -d '+datapath,'ls -Rhaltn '+basepath]:
        chunk=['<h2 style="background-color:#DDF;">Befehl: {}</h2>\n<pre>'.format(command)]
        try:
            stdout=subprocess.check_output(command, shell=True, stderr=subprocess.STDOUT).decode("utf-8", "ignore")
            for line in stdout.split("\n"):
                line=line.replace(' ','&nbsp;')
                chunk.append(line)
            datei.append('\n'.join(chunk)+'\n</pre>')   
        except Exception as e:
            import ti_lib
            datei.append('Befehl konnte nicht ausgeführt werden<br>'+'<br>\n'.join(chunk)+'\n</pre>'+ti_lib.beschreibe_exception(e))

    dump=ti_ctx.cnf.dump_db_config()
    chunk=['<h2>DATENBANK: Konfigurationseinträge</h2>\n']
    chunk.append('<table>')
    for line in dump:
        chunk.append('<tr><td>{}</td><td>{}</td></tr>'.format(line[0],line[1]))
    chunk.append('</table>')
    datei.append('\n'.join(chunk))   
    return "<br>\n<br>\n===============================================================<br>\n<br>\n".join(datei)
    
def do_config(ti_ctx):
    HTML_TEMPLATE = '''
    <div class="firstcontent" style="background:white;margin-bottom:0.5em;">
        <h1>Einstellungen</h1></div>
    <div class="firstcontent" style="background:white;">\n\n{dialog}\n\n</div>'''

    ti_ctx.check_management_rights(konst.RECHTEAdministration)

    debugfileflag=ti_ctx.req.get_cgi('debugfile')
    ti_ctx.res.debug('debugfile',debugfileflag)
    if debugfileflag.lower()=='true':
        df=make_debugfile(ti_ctx).encode('utf-8')
        import lzma, sys
        sys.stdout.buffer.write(b'Content-Type: application/octet-stream\n')
        sys.stdout.buffer.write(b'Content-Disposition: attachment;filename=config.html.xz\n\n')
        sys.stdout.buffer.write(lzma.compress(df))
    else:
        erfolg=config_todo(ti_ctx)
        ti_ctx.res.debug("Erfolg",erfolg)
        if erfolg:
            ti_ctx.set_erfolgsmeldung(erfolg)
        ti_ctx.print_html_head(ismanagement=True)
        ti.prt(HTML_TEMPLATE.format(css=ti_ctx.standardcss(),dialog=config_dialog(ti_ctx)))
        ti_ctx.print_html_tail()

if __name__=='__main__':
    ti_ctx=ti.master_init()
    try:
        do_config(ti_ctx)
    except Exception as e:
        ti_lib.panic("config.py scheiterte",e)
