#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
________________________________________________________________________
 ti_context.py                                                            
 EN: This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß          
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 ti_context.py liefert die gleichnamige Klasse. 
    Deren Aufgabe ist:
        * verfügbarmachen der anderen großen Objekte sys, cnf, res, rew
        * diverse Funktionen durchzuführen, die diesen context nutzen
        
    Da jede größere Methode den ti_ctx für ihren Thread bekommt, 
        kann quasi jeder diesen Context und seine Funktionen nutzen
        ohne sich um Threadsicherheit Gedanken machen zu müssen.
    Hiermit werden final globale Variablen elegant eliminiert!
________________________________________________________________________
'''

# Batteries
import cgitb
cgitb.enable()
# TI
import ti_lib
import ti
try: 
    import konst
    #import client
except Exception as e:
    ti_lib.panic('ti_context.py: IMPORT fehlgeschlagen',e,'')

 
#################################################    
### hier beginnt der Code zur Initialisierung ###
#################################################

class TIConTeXt(object):
    """ Der TI-Context nimmt als Objekt die Zeiger auf alle 
        notwendigen Objekte für die Interaktion mit Request, Response usw. auf.
        Eine Methode, die diesen Context übergeben bekommt kann also 
        auf alle Daten zugreifen bzw. zurückgeben.
        sys: Schnittstelle zu ....
        cnf: Schnittstelle zur Konfigurationsdatenbank
        req: Hält die Request-Daten bereit
        res: Baut die Response zusammen
        ses: verwaltet einen session (Login oder per Verwaltungsnetz)"""
    def __init__(self,sys,cnf,req,res,ses):
        self.sys=sys
        self.cnf=cnf
        self.req=req
        self.res=res
        self.ses=ses
        import s_any # da s_any definitiv noch nicht angepasst 2020-01-13
        self.s_interface=s_any.S_Interface(self)
        
    ### nun die direkt auf diesem Datensatz operierenden Funktionen
    
    def check_management_rights(self,rechte=255):
        # Kunstgriff, um ti_ctx implizit zu übergeben
        return self.ses.check_management_rights_or_exit(self,rechte)
    
    def get_ti_lastupload(self):
        return self.cnf.get_db_config('uploadtimestamp','123')

    def set_ti_lastupload(self):
        self.cnf.set_ti_lastupload()
        
    def get_client_ip_or_name(self):
        # teuer, sollte nur von management-modulen genutzt werden
        #debug("clientname",__ti_clientname__)

        name=self.ses.get_session_user()
        if len(name)>0 and name!='vianetwork':
            return name
        name=ti_lib.client_get_name( self.req.client.ip_address )
        return name

    # liefert (wenn gesetzt) eine Client-spezifische Obergrenze für anzuzeigende
    # Vertretungsplantage, Obergrenze 10 ist hier fest kodiert
    def get_client_phaselimits(self,ip=''):
        if not ip:
            return self.req.client.phaselimits()
        name,timestamp,minphase,maxphase,gruppe=self.cnf.client_get_config(ip)
        return minphase,maxphase

      
    def get_client_max_groups_number(self):
        gruppen,_,_=self.req.client.groups()
        cgn=6
        while cgn>0:
            if gruppen>=1<<(cgn-1):
                return cgn
            cgn-=1
        return 0
        
    def get_checkedsessionid(self):
        self.res.debug("checkedsessionid",str(self.ses.get_session_id()))
        return str(self.ses.get_session_id())

    def set_erfolgsmeldung(self,meldung):
        self.ses.set_session_meldung(meldung)
            
    def print_html_tail(self):
        tail= '''\t</body>\n</html>\n'''
        ti.prt(tail)
        self.res.debug_flush()

        
    def print_html_head(self,*args,**kwargs):
        # Kunstgriff, um ti_ctx implizit zu übergeben
        ti_lib.prt(ti.get_html_head(self,*args,**kwargs))

    ##########################################
    # standardhtml-head erzeugen
    def standardcss(self,ismanagement=False):
        pulsecss='''
        <style type="text/css">
            .hiprio, .hiprio *, *[alt_person|=gelb] , *[alt_person|=red], .s_t_fresh {
                        animation: puls 5s linear -2s infinite;
                -webkit-animation: puls 5s linear -2s infinite;
            }

            @keyframes puls {
                  0%   {  text-shadow: none;} 
                  90%  {  text-shadow: none;} 
                  92%  {  text-shadow: 0   0   0em   yellow; } 
                  95%  {  text-shadow: 0px 0px 0.7em yellow; } 
                  98%  {  text-shadow: 0   0   0em   yellow; } 
                  100% {  text-shadow: none;} 
            }
            @-webkit-keyframes puls {
                  0%   {  text-shadow: none;} 
                  90%  {  text-shadow: none;} 
                  92%  {  text-shadow: 0   0   0em   yellow; } 
                  95%  {  text-shadow: 0px 0px 0.7em yellow; } 
                  98%  {  text-shadow: 0   0   0em   yellow; } 
                  100% {  text-shadow: none;} 
            }
        </style>'''
        standard='''
        <link rel="stylesheet" type="text/css" href="/static/{ti}{hell}.css?q={til}">
        {inline}
        <link rel="stylesheet" type="text/css" href="/local/{ti}.css?q={til}">
        ''' 
        hell='-light' if self.cnf.get_db_config_bool('Flag_HellesThema') else ''
        if ismanagement :
            ti="mgm"
            hell=''
        else:
            ti="ti" 
        if self.req.client.ist_rpi():
            return standard.format(ti=ti,til=self.get_ti_lastupload(), hell=hell, inline='')
        else:
            return standard.format(ti=ti,til=self.get_ti_lastupload(), hell=hell, inline=pulsecss)
            
    
