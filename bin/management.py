#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
________________________________________________________________________
 management.py                                                            
 EN: This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß          
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 management.py erzeugt das Backoffice-Frontend von tabula.info und ermöglicht 
 damit alle Verwaltungsaufgaben
 ________________________________________________________________________
'''

# Batteries
import string
import cgitb
cgitb.enable()
from time import sleep

# TI
import ti
import konst
import login

def do_manage(ti_ctx):
    if ti_ctx.req.get_cgi_bool("getstatus"):
        
        
        origstatus=ti_ctx.req.get_cgi_int("origstatus")
        for loop in range(120): # bis zu zwei Minuten lang auf Änderungen warten lassen ("long polling")
            try:
                status=0
                statusmeldung=[]
                bs=ti_ctx.cnf.get_db_config('background_semaphore')
                if bs!='free':
                    status=1
                    statusmeldung.append('Import ...'+bs.split('.')[0][-3:]+' läuft.')
                if ti_ctx.cnf.get_db_config_bool('s_any_force_convert'):
                    status+=2
                    if bs=='free':
                        statusmeldung.append('Import veranlasst!')
                    else:
                        statusmeldung.append('Erneuter Import geplant!')
                statusmeldung.append('')
                statusmeldung.append('')
                statusmeldung.append('')
                if status!=origstatus:
                    break
                sleep(1) # Time in seconds
            except:
                status=8
                statusmeldung=['Bye','','']
                break
            
        ti.prt('\n'+str(status)+';<span style="background-color:lightgreen">'+ '<br>'.join(statusmeldung[:3]) +'</span>')
        exit(0)
        

    ti_ctx.res.debug("+++++ Starte Management +++++")

    ## kann für wsgi dynamisch übergeben werden...:
    ti_session=ti_ctx.ses

    ti.prt("Content-Type: text/html\n")



    wait=ti_ctx.req.get_cgi_int('wait2jump')


    if ti_ctx.req.get_cgi_bool("menu"):
        ti_ctx.print_html_head(title='tabula.info - Management', \
            extrastyle=konst.MAN_TI_AJAX+'<style type="text/css"> body, div {overflow:auto; margin:0px;} body { padding:0px;} div { padding:2px;}</style>', \
            timeout=300,url='management.py?menu=true',ismanagement=True,keinemeldung=True,onload='getAJAX()')
        menustruct=[
            ('Übersicht',       '',0,0,0),
            ('Gesamtansicht',   'frames.py?phase=0;preview=1',0,0,0),
            ('Anzeigenstatus ', 'ti_tool.py',0,0,0),
            ('Bearbeiten',      '',255,0,0),
            ('Meldungen',       'messages.py?manage_mode=1',konst.RECHTEMeldungen | konst.RECHTEMeldungenerweitert,0,0),
           
            ('Personenruf',     'manage_persons.py',konst.RECHTEPersonenruf,0,0),
            ('Wochenmotto',     'motto.py',konst.RECHTEMeldungen | konst.RECHTEMeldungenerweitert,0,0),
            ('Infoseiten',      'manage_info.py',konst.RECHTEInfoupload | konst.RECHTEInfouploaderweitert,0,0),
            ('Pläne',           's_any_upload.py',konst.RECHTEVertretungsplan,0,0),
            ('Administration',  '',konst.RECHTEAdministration,0,0),
            ('Einstellungen',   'config.py',konst.RECHTEAdministration,0,0),
            ('Clients & Status',    'config_clients.py',konst.RECHTEAdministration,0,0),
            ('Login',           '',0,0,0),
            ('Anmelden',        'login.py',0,0,1),
            ('Abmelden',        'login.py?logout=True',255,1,1),
            ('Passwort ändern', 'login.py?setpassword=True',255,1,0),
        ] # Menütext, Sprungziel, akzeptable Rechte (ein Bit genügt), muss man eingeloggt sein?, Link in _top anzeigen?
        #  ('Meld.-Protokoll', 'messages.py?protokoll_mode=1',konst.RECHTEMeldungen | konst.RECHTEMeldungenerweitert,0,0),
        themenu=['''
        <div class="content">
            <h1 style="text-align:center">
                <img src="http://tabula.info/version/'''+konst.TI_LOGO+'''" >
                <br>tabula.info Verwaltung
            </h1>
            <div id="statusmeldung" style="text-align:center;font-size:80%">status<br>&nbsp;</div>
        </div>''']
        entryskeleton='<div class="content"><p><a href="{menulink}" target="{menutarget}">{menuitem}</a></p><p></p></div>'
        headingskeleton='<div class="content"><h4>{menuitem}</h4><p></p></div>'
        ti_ctx.res.debug('meine Rechte',ti_session.get_alle_rechte(255))
        jumpto='login.py'
        jumpwithrights=0
        for mtupel in menustruct:
            menuitem,menulink,menurights,mustbeloggedin,mustbetop = mtupel
            #ti_ctx.res.debug('menutupel',mtupel)
            ### Drei Arten von Menüeintragen
            if menulink=='':   # Der Abschnittsname, hat natürlich kein Sprungziel
                if ( menurights==0      or ti_session.get_alle_rechte(menurights) ) and \
                    ( not mustbeloggedin    or len(ti_session.get_session_user())>0 ):
                    themenu.append(headingskeleton.format(menuitem=menuitem))
                  
            else: # der normale Menüeintrag
                if ( menurights==0      or ti_session.get_alle_rechte(menurights) ) and \
                    ( not mustbeloggedin    or ti_session.ist_angemeldeter_user() ):
                    themenu.append(entryskeleton.format( \
                        menuitem=menuitem, \
                        menulink=menulink, \
                        menutarget='_top' if mustbetop else 'todo' \
                        ))
                    if ((menurights>0 and jumpwithrights==0) or jumpto=='') and not mustbetop and not mustbeloggedin:
                        jumpto=menulink
                        jumpwithrights=menurights
        ziel=ti_session.get_session_command()
        if ziel=='':
            ti_session.set_session_command(jumpto)
        else:
            ti_session.set_session_command(ziel)
        ti.prt("\n".join(themenu))
        ti_ctx.print_html_tail()
    elif wait:
        ziel=ti_session.get_session_command()
        if ziel=='':
            ti_ctx.print_html_head(timeout=wait,url='management.py?wait2jump='+str(wait+1))
        else:
            ti_ctx.print_html_head(timeout=0,url=ziel)
        if wait<5:
            ti.prt('<span style="color:grey;">Einen Moment bitte...<br><small>...oder links im Menü wählen</small></span>')
        else:
            ti.prt('<span style="color:grey;">Bitte wählen Sie links im Menü</small></span>')
        ti_ctx.print_html_tail()
    else:
        link='management.py?wait2jump=1'
        ti_ctx.print_html_head(frameset=konst.MAN_FRAMES_HTML,ismanagement=True)
        ti_ctx.print_html_tail()
        
if __name__ == '__main__':
    ti_ctx=ti.master_init()        
    try:
        do_manage(ti_ctx)
    except Exception as e:
        ti_lib.panic("management failed!",e)

