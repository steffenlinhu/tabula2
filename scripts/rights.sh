#!/bin/bash
if [[ $(id -u) -ne 0 ]] ; then echo "**** Skript muss als root bzw. mit sudo gestartet werden *****" ; exit 1 ; fi
targetpath=/opt/tabula.info
echo Schritt 3: Dateirechte in $targetpath
    chown -R :www-data  ${targetpath}
    chmod       ug+x    ${targetpath}/tabula2/bin/*.py
    chmod       ug+x    ${targetpath}/tabula2/scripts/*.sh
    chmod -R    ug+w    ${targetpath}/local/www/dyn/
    chmod -R    ug+w    ${targetpath}/local/data

