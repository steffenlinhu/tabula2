#!/bin/bash
#
#________________________________________________________________________
# s_any_pdf_convert.sh                                                       
# EN: This file is part of tabula.info, which is free software under              
#     the terms of the GPL without any warranty - see the file COPYING 
# DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß          
#         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
#________________________________________________________________________
#
# s_any_pdf_convert.sh konvertiert EINE pdf-Datei in EINE png-Datei
# 2 Namen müssen daher übergeben werden
# Die erste Seite des PDF entscheidet, ob die Zuschnitte für Hoch- oder Querformat benutzt werden.

konvertiere() {
    
    echo "s_any_pdf_convert.sh: konvertiert $source nach $target"
    
    name=any_pdf_tmp
    if [ -e ${name}.png ]; then
        rm ${name}.png -f
    fi
    ### um ca. faktor 2 zu große Bilder erzeugen
    ## (für Optimierer: der einzige Schritt mit großem Rechenaufwand)
    #####original convert -density 150x150 +repage -resize x2200 -quality 95   ${source} "${name}.png"
    #####nun direkt per ghostscript
    gs -o ${name}-%01d.png -sDEVICE=png16m -r200 ${source} 

    # sollte das pdf nur aus einer Seite bestehen, so wurde gerade ein anderer
    #  Dateiname genommen - dies korrigiert:
    #if [ -e ${name}.png ]; then
    #    mv ${name}.png ${name}-0.png
    #fi
    # "Ränder entfernen"
    rawwidth=`gm identify -format "%w" ${name}-1.png `
    rawheight=`gm identify -format "%h" ${name}-1.png `
    echo "Seitenverhaeltnis: x/y"  $rawwidth "/" $rawheight
    if [ $rawwidth -lt $rawheight ]; then
        pfix="s_any_pdf_hoch_"
        echo "S_ANY_HOCHFORMAT"
    else
        pfix="s_any_pdf_quer_"
        echo "S_ANY_QUERFORMAT"
    fi
    x_prozent=`${cgipath}ti_tool.py ${pfix}x_prozent`
     x_breite=`${cgipath}ti_tool.py ${pfix}x_breite`
      y_hoehe=`${cgipath}ti_tool.py ${pfix}y_hoehe`
         nord=`${cgipath}ti_tool.py ${pfix}abschneiden_nord_p`
          ost=`${cgipath}ti_tool.py ${pfix}abschneiden_ost_p`
         sued=`${cgipath}ti_tool.py ${pfix}abschneiden_sued_p`
         west=`${cgipath}ti_tool.py ${pfix}abschneiden_west_p`
    chopit="-gravity North -chop 0x${nord}% +repage -gravity South -chop 0x${sued}% +repage -gravity West -chop ${west}%x0 +repage -gravity East -chop ${ost}%x0 +repage "
    
    for zaehler in {1..9}; do
        if [ -e ${name}-${zaehler}.png ]; then
            gm convert  ${name}-${zaehler}.png ${chopit}  -resize ${x_prozent}%x100% -blur 1x1 -bordercolor white  -border 1x1 -quality 95 ${name}-${zaehler}.png
        fi
    done
    # größenänderung für die nebeneinandergehängten bilder fester Größe
    for zaehler in {1..9}; do
        if [ -e ${name}-${zaehler}.png ]; then
            # original: convert  ${name}-${zaehler}.png -resize ${x_breite}x${y_hoehe} -size ${x_breite}x${y_hoehe} xc:white +swap -gravity center -composite ${name}-${zaehler}-breite.png
            gm convert  ${name}-${zaehler}.png -resize ${x_breite}x${y_hoehe} -background white -gravity center -extent  ${x_breite}x${y_hoehe} ${name}-${zaehler}-breite.png
            seitenzahl=${zaehler}
        fi
    done
    # nun ein großes Bild mit allen nebeneinander produzieren 
    # "Alle PNGs nebeneinander (mit Schaerfen)"
    gm convert ${name}-*-breite.png +append -sharpen 0x0.7 -quality 95  -depth 8 ${name}.png
    mv ${name}.png ${target}
    rm ${name}-*.png -f
}
    
#################
# Programmstart #
#################

scriptpath=`dirname $(readlink -f ${0})`/
basepath=`realpath ${scriptpath}..`/
cgipath=${basepath}bin/
logpath=${basepath}data/log/
cd ${scriptpath}../../local/data/tmp

source=${1}
target=${2}
echo $0 $1 $2

konvertiere ${source} ${target} 
# Jetzt wird noch schnell die Bildgröße ermittelt
width=`gm identify -format "%w" ${target} `
echo "S_ANY_BREITE ${width} "
echo "Seitenzahl: $(($seitenzahl+1))"
#echo "Geschafft"
